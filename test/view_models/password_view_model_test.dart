// Dart imports:
import 'dart:convert';

// Flutter imports:
import 'package:flutter/cupertino.dart';

// Package imports:
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';

// Project imports:
import 'package:nextcloud_password_client/utils/data_access_layer.dart';
import 'package:nextcloud_password_client/view_models/password_share_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_view_model.dart';
import 'password_view_model_test.mocks.dart';

@GenerateMocks([DataAccessLayer, BuildContext])
void main() {
  MockDataAccessLayer dataAccessLayer = MockDataAccessLayer();

  PasswordShareViewModel passwordShareView = PasswordShareViewModel();
  passwordShareView.created = 20220215050423;
  passwordShareView.editable = false;
  passwordShareView.expires = 20230123043123;
  passwordShareView.id = 'fae0bb8b-1234-ertf-45df-64ded6771525';
  passwordShareView.owner = 'you';
  passwordShareView.receiver = 'me';
  passwordShareView.refreshModel(
      fromModel: passwordShareView,
      toModel: passwordShareView.passwordShareModel);

  PasswordViewModel passwordViewModel = PasswordViewModel();
  passwordViewModel.id = 'b92dfd4f-e9cd-479a-9dd8-a01b0cec2a76';
  passwordViewModel.revision = '28f6ccc9-5443-40a9-9da2-bdad1ed60d55';
  passwordViewModel.label = 'Amazon';
  passwordViewModel.username = 'bezos';
  passwordViewModel.password = '"Mom€ntousB@llyhoo';
  passwordViewModel.notes =
      'One of the largest online shopping websites in the world. The site is widely known for its wide selection of books, although the site has expanded to sell electronics, music, furniture, and apparel. Similar to eBay, users can also purchase and sell items using Amazon\'s online marketplace system. Amazon was founded in 1995 by Jeff Bezos and is based out of Seattle, Washington.';
  passwordViewModel.url = 'https://www.amazon.com';
  passwordViewModel.edited = 1555169699;
  passwordViewModel.favorite = true;
  passwordViewModel.folder = 'fb8ab5f8-52ba-4c44-90a0-276a672e012d';
  passwordViewModel.tags = [
    'bec53e51-e556-4593-9445-2dd15908ba11',
    'b71c274e-7076-4610-b261-6de6b32d426e'
  ];
  passwordViewModel.customFields =
      '[{label: Amaton Prime Pin, type: secret, value: 1234, id: 0, blank: false}, {label: E-Mail, type: email, value: mail@amazon.com, id: 1, blank: false}]';
  passwordViewModel.passwordShareView = passwordShareView;
  passwordViewModel.passwordModel.share = passwordShareView.passwordShareModel;
  passwordViewModel.refreshDataModel(passwordViewModel);

  test('fromMap', () {
    PasswordViewModel passwordViewModelFromMap =
        PasswordViewModel.fromMap(dataAccessLayer.samples['passwords'][3]);
    passwordViewModelFromMap.passwordShareView = passwordShareView;
    passwordViewModelFromMap.passwordModel.share =
        passwordShareView.passwordShareModel;
    expect(passwordViewModelFromMap, passwordViewModel);
  });

  test('fromModel', () {
    PasswordViewModel passwordViewModelFromModel =
        PasswordViewModel.fromModel(passwordViewModel.passwordModel);
    expect(passwordViewModelFromModel, passwordViewModel);
  });

  test('refreshDataModel', () {
    passwordViewModel.client = 'TestClient';
    expect(passwordViewModel.passwordModel.props == passwordViewModel.props,
        false);

    passwordViewModel.refreshDataModel(passwordViewModel);
    expect(passwordViewModel.passwordModel.props, passwordViewModel.props);
  });

  test('refreshViewModel', () {
    passwordViewModel.passwordModel.trashed = true;
    expect(passwordViewModel.passwordModel.props == passwordViewModel.props,
        false);

    passwordViewModel.refreshViewModel(passwordViewModel.passwordModel);
    expect(passwordViewModel.passwordModel.props, passwordViewModel.props);
  });

  test("getBodyForFavoriteUpdate", () {
    String body = jsonEncode({
      'id': passwordViewModel.id,
      'password': passwordViewModel.password,
      'label': passwordViewModel.label,
      'hash': passwordViewModel.hash,
      'cseType': passwordViewModel.cseType,
      'cseKey': passwordViewModel.cseKey,
      'favorite': passwordViewModel.favorite
    });
    expect(body, passwordViewModel.getBodyForFavoriteUpdate());
  });

  test("getBodyForTagUpdate", () {
    String body = jsonEncode({
      'id': passwordViewModel.id,
      'password': passwordViewModel.password,
      'label': passwordViewModel.label,
      'hash': passwordViewModel.hash,
      'cseType': passwordViewModel.cseType,
      'cseKey': passwordViewModel.cseKey,
      'tags': passwordViewModel.tags
    });
    expect(body, passwordViewModel.getBodyForTagUpdate());
  });
}
