// Package imports:
import 'package:flutter_test/flutter_test.dart';

// Project imports:
import 'package:nextcloud_password_client/enums/themes.dart';
import 'package:nextcloud_password_client/models/config_model.dart';

void main() {
  test("getUseLocalCopy", () {
    ConfigModel model = ConfigModel();
    expect(model.useLocalCopy, false);
    model.$useLocalCopy = true;
    expect(model.useLocalCopy, true);
  });
  test("saveMasterPassword", () {
    ConfigModel model = ConfigModel();
    expect(model.saveMasterPassword, false);
    model.$saveMasterPassword = true;
    expect(model.saveMasterPassword, true);
  });

  test("getSelectedTheme", () {
    ConfigModel model = ConfigModel();
    expect(model.selectedTheme, Themes.nextcloud);
    model.$selectedTheme = Themes.light;
    expect(model.selectedTheme, Themes.light);
  });
}
