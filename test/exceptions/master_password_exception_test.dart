// Package imports:
import 'package:flutter_test/flutter_test.dart';

// Project imports:
import 'package:nextcloud_password_client/exceptions/master_password_exception.dart';

void main() {
  test('Credentials Exception message', () {
    MasterPasswordException exception =
        MasterPasswordException('master password is incorrect', 654);

    expect(exception.toString(),
        "Message : master password is incorrect, Error code 654");

    expect(exception.toString() != "You have to correct the master password",
        true);
  });
}
