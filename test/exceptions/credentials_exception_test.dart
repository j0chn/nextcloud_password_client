// Package imports:
import 'package:flutter_test/flutter_test.dart';

// Project imports:
import 'package:nextcloud_password_client/exceptions/credentials_exception.dart';

void main() {
  test('Credentials Exception message', () {
    CredentialsException exception =
        CredentialsException('Wrong credentials entered', 111);

    expect(exception.toString(),
        "Message : Wrong credentials entered, Error code 111");

    expect(
        exception.toString() !=
            "Message : Wrong credentials entered with error code 111",
        true);
  });
}
