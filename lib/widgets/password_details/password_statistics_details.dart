// Flutter imports:
import 'package:flutter/cupertino.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/utils/key_chain.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/folder_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/folder_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_view_model.dart';
import 'package:nextcloud_password_client/widgets/formatted_date.dart';

Widget passwordStatisticsDetails(
    BuildContext context, PasswordViewModel password) {
  KeyChain keyChain = context.read<ConfigViewModel>().keyChain;

  FolderViewModel folder =
      context.read<FolderListViewModel>().getFolderById(password.folder);

  String created = formattedDate(password.created.toString());

  String lastEdited = formattedDate(password.edited.toString());

  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(
        AppLocalizations.of(context)!.statistics,
        style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
      ),
      Row(
        children: [
          Text(AppLocalizations.of(context)!.created),
          Text(created),
        ],
      ),
      Row(
        children: [
          Text(AppLocalizations.of(context)!.lastEdited),
          Text(lastEdited),
        ],
      ),
      Row(
        children: [
          Text(AppLocalizations.of(context)!.id),
          Text(password.id),
        ],
      ),
      Row(
        children: [
          Text(AppLocalizations.of(context)!.folder),
          Text(keyChain.decrypt(folder.cseKey, folder.label)),
        ],
      ),
      Row(
        children: [
          Text(AppLocalizations.of(context)!.revisionColumn),
          Text('${password.revisions.length} revisions'),
        ],
      ),
      Row(
        children: [
          Text(AppLocalizations.of(context)!.sharedColumn),
          Text('${password.shareViewModels.length} shares'),
        ],
      ),
    ],
  );
}
