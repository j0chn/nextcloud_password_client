// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

// Project imports:
import 'package:nextcloud_password_client/view_models/password_view_model.dart';

Widget passwordSecurityDetails(
    BuildContext context, PasswordViewModel password) {
  late Text statusText;

  late String sseText;

  switch (password.statusCode) {
    case 'BREACHED':
      statusText = Text(
        AppLocalizations.of(context)!.breached,
        style: const TextStyle(color: Colors.red),
      );
      break;

    case 'OUTDATED':
      statusText = Text(
        AppLocalizations.of(context)!.expired,
        style: const TextStyle(color: Colors.amber),
      );
      break;

    case 'DUPLICATE':
      statusText = Text(
        AppLocalizations.of(context)!.duplicate,
        style: const TextStyle(color: Colors.amber),
      );
      break;

    case 'GOOD':
    default:
      statusText = Text(
        AppLocalizations.of(context)!.secure,
        style: const TextStyle(color: Colors.green),
      );
      break;
  }

  switch (password.sseType) {
    case 'SSEv1r1':
      sseText = AppLocalizations.of(context)!.sse1r1;
      break;

    case 'SSEv1r2':
      sseText = AppLocalizations.of(context)!.ssev1r2;
      break;

    case 'SSEv2':
      sseText = AppLocalizations.of(context)!.ssev2;
      break;

    case 'none':
    default:
      sseText = AppLocalizations.of(context)!.noEncryption;
      break;
  }

  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(
        AppLocalizations.of(context)!.security,
        style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
      ),
      Row(
        children: [
          Text(AppLocalizations.of(context)!.statusColumn),
          statusText,
        ],
      ),
      Row(
        children: [
          Text(AppLocalizations.of(context)!.serverEncryption),
          Text(sseText),
        ],
      ),
      Row(
        children: [
          Text(AppLocalizations.of(context)!.clientEncryption),
          password.cseType == 'none'
              ? Text(AppLocalizations.of(context)!.noEncryption)
              : const Text('libsodium'),
        ],
      ),
      Row(
        children: [
          const Text('sha1'),
          Text(password.hash),
        ],
      ),
    ],
  );
}
