// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/utils/key_chain.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_view_model.dart';
import 'package:nextcloud_password_client/view_models/tag_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/tag_view_model.dart';

Widget passwordTags(PasswordViewModel password) {
  return ListView.builder(
    shrinkWrap: true,
    scrollDirection: Axis.horizontal,
    itemCount: password.tags.length,
    itemBuilder: (BuildContext context, int index) {
      KeyChain keyChain = context.read<ConfigViewModel>().keyChain;

      TagViewModel tagViewModel = context
          .read<TagListViewModel>()
          .getTagById(password.tags.elementAt(index));

      String label = keyChain.decrypt(tagViewModel.cseKey, tagViewModel.label);

      return ListTile(
//onTap: () => context.read<ViewState>().selectedSettingsItem =
//passwordViewModel.revisions.elementAt(index),
        title: Text(
          label,
          style: TextStyle(
            color: Color(
              int.parse(
                tagViewModel.color.replaceFirst('#', '0xff'),
              ),
            ),
          ),
        ),
      );
    },
  );
}
