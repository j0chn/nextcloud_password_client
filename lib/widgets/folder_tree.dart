// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_simple_treeview/flutter_simple_treeview.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:nextcloud_password_client/widgets/dialogues/folder_editing_dialogue.dart';
import 'package:nextcloud_password_client/widgets/dialogues/tag_editing_dialogue.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/api_constants.dart';
import 'package:nextcloud_password_client/constants/folder_keys.dart';
import 'package:nextcloud_password_client/constants/widget_constants.dart';
import 'package:nextcloud_password_client/enums/nodes.dart';
import 'package:nextcloud_password_client/enums/security.dart';
import 'package:nextcloud_password_client/utils/key_chain.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/folder_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/folder_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/tag_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/tag_view_model.dart';
import 'package:nextcloud_password_client/widgets/favorite_button.dart';

Widget createTree(BuildContext context) {
  final TreeController treeController = TreeController(allNodesExpanded: false);
  final ScrollController detailScrollController = ScrollController();
  final ScrollController detailScrollController2 = ScrollController();

  SingleChildScrollView singleChildScrollView = SingleChildScrollView(
    controller: detailScrollController,
    scrollDirection: Axis.vertical,
    child: SingleChildScrollView(
      controller: detailScrollController2,
      scrollDirection: Axis.horizontal,
      child: TreeView(
        iconSize: 20,
        treeController: treeController,
        nodes: _createAllNodes(
          context.watch<FolderListViewModel>().folderViewModels,
          apiRootFolder,
          context,
        ),
      ),
    ),
  );

  ViewState viewState = context.read<ViewState>();
  FolderListViewModel folderListViewModel = context.read<FolderListViewModel>();
  List<String> parentFolders = [];

  switch (viewState.selectedNode) {
    case Nodes.folder:
      parentFolders =
          folderListViewModel.getParentFolders(viewState.selectedFolder);
      treeController.expandNode(const ValueKey(apiRootFolder));

      for (String folder in parentFolders) {
        treeController.expandNode(ValueKey(folder));
      }
      break;

    case Nodes.favorite:
      treeController.expandNode(const ValueKey('favorite'));

      if (viewState.selectedFolder.isNotEmpty) {
        parentFolders =
            folderListViewModel.getParentFolders(viewState.selectedFolder);
        treeController
            .expandNode(ValueKey('${viewState.selectedFolder}favorite'));
      } else if (viewState.selectedTag.isNotEmpty) {
        treeController.expandNode(ValueKey('${viewState.selectedTag}favorite'));
      }

      break;

    case Nodes.shared:
      treeController.expandNode(const ValueKey("shares"));
      break;

    case Nodes.tag:
      treeController.expandNode(const ValueKey('tags'));
      break;

    case Nodes.security:
      treeController.expandNode(const ValueKey('security'));
      break;

    case Nodes.all:
      break;

    case Nodes.recent:
      break;
  }

  return singleChildScrollView;
}

Widget _nodeRow(
    BuildContext context, Icon icon, String id, String text, String type,
    {FavoriteIcon? favIcon}) {
  Color color = Colors.white;
  ViewState viewState = context.read<ViewState>();

  if (id == viewState.selectedFavorite ||
      (id == favoritePasswords &&
          viewState.selectedNode == Nodes.favorite &&
          viewState.selectedTag.isEmpty) ||
      (id == apiRootFolder && viewState.selectedNode == Nodes.all) ||
      (id == viewState.selectedFolder &&
              viewState.selectedNode == Nodes.folder) &&
          viewState.selectedFolder != apiRootFolder ||
      id == viewState.selectedPassword ||
      id == viewState.selectedTag ||
      (id == recent && viewState.selectedNode == Nodes.recent) ||
      (id == sharedWithYou &&
          viewState.selectedNode == Nodes.shared &&
          viewState.sharedByYou == false) ||
      (id == sharedByYou &&
          viewState.selectedNode == Nodes.shared &&
          viewState.sharedByYou == true) ||
      (id == secure &&
          viewState.selectedNode == Nodes.security &&
          viewState.selectedSecurity == Security.secure) ||
      (id == weak &&
          viewState.selectedNode == Nodes.security &&
          viewState.selectedSecurity == Security.weak) ||
      (id == breached &&
          viewState.selectedNode == Nodes.security &&
          viewState.selectedSecurity == Security.breached)) {
    color = Colors.grey.shade300;
  }

  return Container(
      color: color,
      child: Row(mainAxisSize: MainAxisSize.min, children: [
        favIcon ?? const SizedBox.shrink(),
        icon,
        const SizedBox(width: 5),
        Text(text),
        _nodeMenubutton(context, id, type),
      ]));
}

_nodeMenubutton(BuildContext context, String id, String type) {
  Widget widget;
  switch (type) {
    case folder:
      if (id == apiRootFolder) {
        widget = const SizedBox.shrink();
      } else {
        widget = PopupMenuButton<String>(
          onSelected: (value) => {
            if (value == 'deleteFolder')
              {
                context
                    .read<FolderListViewModel>()
                    .delete(context, folderId: id)
              }
            else if (value == 'editFolder')
              {FolderEditingDialogue.showFolderEditingDialogue(context, id)}
          },
          itemBuilder: (context) => <PopupMenuEntry<String>>[
            PopupMenuItem<String>(
              value: 'deleteFolder',
              child: ListTile(
                title: Text(AppLocalizations.of(context)!.deleteFolder),
              ),
            ),
            PopupMenuItem<String>(
              value: 'editFolder',
              child: ListTile(
                title: Text(AppLocalizations.of(context)!.editFolder),
              ),
            ),
          ],
        );
      }
      break;
    case tags:
      TagListViewModel tagListViewModel;
      TagViewModel tagViewModel;
      widget = PopupMenuButton<String>(
        onSelected: (value) => {
          if (value == 'deleteTag')
            {
              tagListViewModel = context.read<TagListViewModel>(),
              tagViewModel = tagListViewModel.getTagById(id),
              context.read<TagListViewModel>().removeTag(tagViewModel),
              context.read<PasswordListViewModel>().removeTag(tagViewModel)
            }
          else if (value == 'editTag')
            {TagEditingDialogue.showTagEditingDialogue(context, id)}
        },
        itemBuilder: (context) => <PopupMenuEntry<String>>[
          PopupMenuItem<String>(
            value: 'deleteTag',
            child: ListTile(
              title: Text(AppLocalizations.of(context)!.deleteTag),
            ),
          ),
          PopupMenuItem<String>(
            value: 'editTag',
            child: ListTile(
              title: Text(AppLocalizations.of(context)!.editTag),
            ),
          )
        ],
      );
      break;
    default:
      widget = const SizedBox.shrink();
      break;
  }
  return widget;
}

List<TreeNode> _createAllNodes(
    Map<String, List<FolderViewModel>> folderViewModels,
    String apiRootFolder,
    BuildContext context) {
  List<TreeNode> allNodes = [];
  allNodes.add(
    TreeNode(
      content: MaterialButton(
        onPressed: () {
          ViewState viewState = context.read<ViewState>();
          viewState.selectedFolder = apiRootFolder;
          viewState.selectedPassword = '';
          viewState.selectedFavorite = '';
          viewState.sharedByYou = true;
          viewState.selectedSecurity = Security.secure;
          viewState.selectedTag = '';
          viewState.selectedNode = Nodes.all;
          viewState.persistViewState();
          context
              .read<PasswordListViewModel>()
              .setPasswordsByFolder(context, apiRootFolder);
        },
        child: _nodeRow(
            context,
            Icon(MdiIcons.earth),
            apiRootFolder,
            '${AppLocalizations.of(context)!.all} (${context.read<PasswordListViewModel>().numberOfFolderPasswords[apiRootFolder]})',
            root),
      ),
      key: const ValueKey(root),
    ),
  );
  allNodes
      .addAll(_createFolders(folderViewModels, apiRootFolder, context, false));
  allNodes.add(
    TreeNode(
      content: MaterialButton(
        onPressed: () {
          ViewState viewState = context.read<ViewState>();
          viewState.selectedFolder = '';
          viewState.selectedPassword = '';
          viewState.selectedFavorite = '';
          viewState.sharedByYou = true;
          viewState.selectedSecurity = Security.secure;
          viewState.selectedTag = '';
          viewState.selectedNode = Nodes.recent;
          viewState.persistViewState();
          context.read<PasswordListViewModel>().setRecentPasswords(context);
        },
        child: _nodeRow(context, const Icon(Icons.timelapse_rounded), recent,
            AppLocalizations.of(context)!.recent, recent),
      ),
      key: const ValueKey(recent),
    ),
  );

  allNodes.add(
    TreeNode(
      children: _getFavoriteNodes(context),
      content: MaterialButton(
        onPressed: () {},
        child: _nodeRow(context, const Icon(Icons.star), favorites,
            AppLocalizations.of(context)!.favorites, favorites),
      ),
      key: const ValueKey(favorites),
    ),
  );

  allNodes.add(
    TreeNode(
      children: _getShareNodes(context),
      content: MaterialButton(
        onPressed: () {},
        child: _nodeRow(context, const Icon(Icons.share), shares,
            AppLocalizations.of(context)!.shared, shares),
      ),
      key: const ValueKey(shares),
    ),
  );

  allNodes.add(
    TreeNode(
      children: _getTagNodes(
          context, context.read<TagListViewModel>().tagViewModels, false),
      content: MaterialButton(
        onPressed: () {},
        child: _nodeRow(context, Icon(MdiIcons.tagMultiple), tags,
            AppLocalizations.of(context)!.tags, tagsRoot),
      ),
      key: const ValueKey(tags),
    ),
  );

  allNodes.add(
    TreeNode(
      children: _getSecurityNodes(context),
      content: MaterialButton(
        onPressed: () {},
        child: _nodeRow(context, Icon(MdiIcons.shieldHalfFull), security,
            AppLocalizations.of(context)!.security, security),
      ),
      key: const ValueKey(security),
    ),
  );

  return allNodes;
}

_getSecurityNodes(BuildContext context) {
  List<TreeNode> treeNodes = [];
  treeNodes.add(
    TreeNode(
      content: MaterialButton(
        onPressed: () {
          ViewState viewState = context.read<ViewState>();
          viewState.selectedFolder = '';
          viewState.selectedPassword = '';
          viewState.selectedFavorite = '';
          viewState.sharedByYou = true;
          viewState.selectedSecurity = Security.secure;
          viewState.selectedTag = '';
          viewState.selectedNode = Nodes.security;
          viewState.persistViewState();
          context
              .read<PasswordListViewModel>()
              .setSecurePasswords(context, Security.secure);
        },
        child: _nodeRow(
            context,
            Icon(MdiIcons.shieldHalfFull, color: Colors.green),
            secure,
            '${AppLocalizations.of(context)!.secure} (${context.read<PasswordListViewModel>().numberOfSecurePasswords})',
            secure),
      ),
      key: const ValueKey(secure),
    ),
  );
  treeNodes.add(
    TreeNode(
      content: MaterialButton(
        onPressed: () {
          ViewState viewState = context.read<ViewState>();
          viewState.selectedFolder = '';
          viewState.selectedPassword = '';
          viewState.selectedFavorite = '';
          viewState.sharedByYou = true;
          viewState.selectedSecurity = Security.weak;
          viewState.selectedTag = '';
          viewState.selectedNode = Nodes.security;
          viewState.persistViewState();
          context
              .read<PasswordListViewModel>()
              .setSecurePasswords(context, Security.weak);
        },
        child: _nodeRow(
            context,
            Icon(MdiIcons.shieldHalfFull, color: Colors.amber),
            weak,
            '${AppLocalizations.of(context)!.weak} (${context.read<PasswordListViewModel>().numberOfWeakPasswords})',
            weak),
      ),
      key: const ValueKey(weak),
    ),
  );
  treeNodes.add(
    TreeNode(
      content: MaterialButton(
        onPressed: () {
          ViewState viewState = context.read<ViewState>();
          viewState.selectedFolder = '';
          viewState.selectedPassword = '';
          viewState.selectedFavorite = '';
          viewState.sharedByYou = true;
          viewState.selectedSecurity = Security.breached;
          viewState.selectedTag = '';
          viewState.selectedNode = Nodes.security;
          viewState.persistViewState();
          context
              .read<PasswordListViewModel>()
              .setSecurePasswords(context, Security.breached);
        },
        child: _nodeRow(
            context,
            Icon(MdiIcons.shieldHalfFull, color: Colors.red),
            breached,
            '${AppLocalizations.of(context)!.breached} (${context.read<PasswordListViewModel>().numberOfBreachedPasswords})',
            breached),
      ),
      key: const ValueKey(breached),
    ),
  );
  return treeNodes;
}

_getTagNodes(
    BuildContext context, List<TagViewModel> tagsModels, bool favorite) {
  KeyChain keyChain = context.read<ConfigViewModel>().keyChain;
  List<TreeNode> treeNodes = [];
  for (TagViewModel tagViewModel in tagsModels) {
    treeNodes.add(
      TreeNode(
        content: MaterialButton(
          onPressed: () {
            ViewState viewState = context.read<ViewState>();
            viewState.selectedFolder = '';
            viewState.selectedPassword = '';
            viewState.selectedFavorite = '';
            viewState.sharedByYou = true;
            viewState.selectedSecurity = Security.secure;
            viewState.selectedTag = tagViewModel.id;
            viewState.selectedNode = Nodes.tag;
            viewState.persistViewState();
            context
                .read<PasswordListViewModel>()
                .setPasswordsByTag(context, tagViewModel.id);
          },
          child: _nodeRow(
              context,
              Icon(
                MdiIcons.tag,
                color: Color(
                  int.parse(
                    keyChain
                        .decrypt(tagViewModel.cseKey, tagViewModel.color)
                        .replaceFirst(colorHashTag, colorPrefix),
                  ),
                ),
              ),
              tagViewModel.id,
              '${keyChain.decrypt(tagViewModel.cseKey, tagViewModel.label)} (${context.read<PasswordListViewModel>().numberOfTaggedPasswords[tagViewModel.id]})',
              tags),
        ),
        key: favorite
            ? ValueKey('${tagViewModel.id}favorite')
            : ValueKey(tagViewModel.id),
      ),
    );
  }
  return treeNodes;
}

List<TreeNode> _getShareNodes(BuildContext context) {
  List<TreeNode> treeNodes = [];
  treeNodes.add(
    TreeNode(
      content: MaterialButton(
        onPressed: () {
          context
              .read<PasswordListViewModel>()
              .setPasswordsByShare(context, false);
          ViewState viewState = context.read<ViewState>();
          viewState.selectedFolder = '';
          viewState.selectedPassword = '';
          viewState.selectedFavorite = '';
          viewState.sharedByYou = false;
          viewState.selectedSecurity = Security.secure;
          viewState.selectedTag = '';
          viewState.selectedNode = Nodes.shared;
          viewState.persistViewState();
        },
        child: _nodeRow(
            context,
            const Icon(Icons.share),
            sharedWithYou,
            '${AppLocalizations.of(context)!.sharedWithYou} (${context.read<PasswordListViewModel>().numberOfSharedPasswordsWithYou})',
            sharedWithYou),
      ),
      key: const ValueKey(sharedWithYou),
    ),
  );
  treeNodes.add(
    TreeNode(
      content: MaterialButton(
        onPressed: () {
          context
              .read<PasswordListViewModel>()
              .setPasswordsByShare(context, true);
          ViewState viewState = context.read<ViewState>();
          viewState.selectedFolder = '';
          viewState.selectedPassword = '';
          viewState.selectedFavorite = '';
          viewState.sharedByYou = true;
          viewState.selectedSecurity = Security.secure;
          viewState.selectedTag = '';
          viewState.selectedNode = Nodes.shared;
          viewState.persistViewState();
        },
        child: _nodeRow(
            context,
            const Icon(Icons.share),
            sharedByYou,
            '${AppLocalizations.of(context)!.sharedByYou} (${context.read<PasswordListViewModel>().numberOfSharedPasswordsByYou})',
            sharedByYou),
      ),
      key: const ValueKey(sharedByYou),
    ),
  );
  return treeNodes;
}

List<TreeNode> _getFavoriteNodes(BuildContext context) {
  List<TreeNode> treeNodes = [];
  List<FolderViewModel> folderViewModels =
      context.read<FolderListViewModel>().getFavoriteFolder();
  List<TagViewModel> tagViewModels =
      context.read<TagListViewModel>().getFavoriteTags();

  treeNodes.add(
    TreeNode(
      content: MaterialButton(
        onPressed: () {
          ViewState viewState = context.read<ViewState>();
          viewState.selectedFolder = '';
          viewState.selectedPassword = '';
          viewState.selectedFavorite = '';
          viewState.sharedByYou = true;
          viewState.selectedSecurity = Security.secure;
          viewState.selectedTag = '';
          viewState.selectedNode = Nodes.favorite;
          viewState.persistViewState();
          context.read<PasswordListViewModel>().setFavoritePasswords(context);
        },
        child: _nodeRow(context, const Icon(Icons.vpn_key), favoritePasswords,
            AppLocalizations.of(context)!.passwords, favoritePasswords),
      ),
      key: const ValueKey(favoritePasswords),
    ),
  );
  for (FolderViewModel folderViewModel in folderViewModels) {
    treeNodes.addAll(_createFolders({}, folderViewModel.id, context, true));
  }

  treeNodes.addAll(_getTagNodes(context, tagViewModels, true));
  return treeNodes;
}

List<TreeNode> _createFolders(Map<String, List<FolderViewModel>> folderMap,
    String currentFolder, BuildContext context, bool favorite) {
  List<TreeNode> currentLevelNodes = [];
  if (folderMap.containsKey(currentFolder)) {
    for (var folder in folderMap[currentFolder]!) {
      currentLevelNodes.addAll(
        _createFolders(folderMap, folder.id, context, favorite),
      );
    }
  }
  FolderViewModel folderViewModel =
      context.read<FolderListViewModel>().getFolderById(currentFolder);
  if (folderViewModel.id.isEmpty) {
    folderViewModel.id = apiRootFolder;
    folderViewModel.label = AppLocalizations.of(context)!.folder;
  }
  return [
    TreeNode(
      children: currentLevelNodes,
      content: MaterialButton(
        onPressed: () {
          ViewState viewState = context.read<ViewState>();
          viewState.selectedFolder = currentFolder;
          viewState.selectedPassword = '';
          viewState.selectedFavorite = '';
          viewState.sharedByYou = true;
          viewState.selectedSecurity = Security.secure;
          viewState.selectedTag = '';
          viewState.selectedNode = Nodes.folder;
          viewState.persistViewState();
          context
              .read<PasswordListViewModel>()
              .setPasswordsByFolder(context, currentFolder);
        },
        child: _nodeRow(
            context,
            const Icon(Icons.folder),
            folderViewModel.id,
            '${context.read<ConfigViewModel>().keyChain.decrypt(folderViewModel.cseKey, folderViewModel.label)} (${context.read<PasswordListViewModel>().numberOfFolderPasswords[folderViewModel.id]})',
            folder,
            favIcon: folderViewModel.id != apiRootFolder
                ? FavoriteIcon(folderViewModel)
                : null),
      ),
      key: favorite
          ? ValueKey('${folderViewModel.id}favorite')
          : ValueKey(folderViewModel.id),
    )
  ];
}
