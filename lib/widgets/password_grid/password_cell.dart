// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:pluto_grid_plus/pluto_grid_plus.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/password_grid_header_constants.dart';
import 'package:nextcloud_password_client/utils/key_chain.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_view_model.dart';

Map<String, PlutoCell> getPasswordCells(
    BuildContext context, PasswordViewModel passwordViewModel) {
  KeyChain keyChain = context.read<ConfigViewModel>().keyChain;

  Map<String, PlutoCell> cells = {};

  cells[label] = PlutoCell(
      value:
          '${passwordViewModel.favIconString}~${keyChain.decrypt(passwordViewModel.cseKey, passwordViewModel.label)}');

  cells[userName] = PlutoCell(
      value: keyChain.decrypt(
          passwordViewModel.cseKey, passwordViewModel.username));

  cells[password] = PlutoCell(
      value: keyChain.decrypt(
          passwordViewModel.cseKey, passwordViewModel.password));

  cells[url] = PlutoCell(
      value: keyChain.decrypt(passwordViewModel.cseKey, passwordViewModel.url));

  cells[shared] = PlutoCell(value: passwordViewModel.shared);

  cells[status] = PlutoCell(value: passwordViewModel.status);

  cells[options] = PlutoCell(value: '');

  return cells;
}
