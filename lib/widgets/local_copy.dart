// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/view_models/config_view_model.dart';

Widget localCopyWidgets(BuildContext context) {
  return Container(
    margin: const EdgeInsets.symmetric(vertical: 10),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Expanded(child: _localCopyText(context)),
        _localCopyCheckBox(context),
      ],
    ),
  );
}

Widget _localCopyText(BuildContext context) {
  return Text(AppLocalizations.of(context)!.createLocalCopy);
}

Widget _localCopyCheckBox(BuildContext context) {
  bool useLocalCopy = context.watch<ConfigViewModel>().useLocalCopy;

  return Checkbox(
    checkColor: Colors.white,
    value: useLocalCopy,
    onChanged: (value) {
      context.read<ConfigViewModel>().useLocalCopy = !useLocalCopy;
    },
  );
}
