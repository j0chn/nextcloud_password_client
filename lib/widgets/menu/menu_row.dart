// Flutter imports:
import 'package:flutter/material.dart';
import 'package:nextcloud_password_client/widgets/menu/tag_menu.dart';

// Package imports:
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/widgets/menu/folder_menu.dart';
import 'package:nextcloud_password_client/widgets/menu/help_menu.dart';
import 'package:nextcloud_password_client/widgets/menu/password_menu.dart';
import 'package:nextcloud_password_client/widgets/menu/system_menu.dart';

Widget getMenuBar(BuildContext context) {
  return Row(
    children: [
      Container(
        margin: const EdgeInsets.symmetric(horizontal: 5),
        child: getPasswordMenu(context),
      ),
      Container(
        margin: const EdgeInsets.symmetric(horizontal: 5),
        child: getFolderMenu(context),
      ),
      Container(
        margin: const EdgeInsets.symmetric(horizontal: 5),
        child: getTagMenu(context),
      ),
      Container(
        margin: const EdgeInsets.symmetric(horizontal: 5),
        child: getSystemMenu(context),
      ),
      Container(
        margin: const EdgeInsets.symmetric(horizontal: 5),
        child: getHelpMenu(context),
      ),
    ],
  );
}

openMenu(BuildContext context, String value) {
  context.read<ViewState>().showSettings();
}
