// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/menu_constants.dart';
import 'package:nextcloud_password_client/widgets/menu/selection_handler.dart';

class MyPopUpMenu<T> extends PopupMenuButton {
  final BuildContext context;

  const MyPopUpMenu(this.context,
      {super.key, required super.itemBuilder, super.tooltip, super.child});

  @override
  PopupMenuItemSelected? get onSelected =>
      (value) => handleMenuItemSelection(context, value);
}

Widget getSystemMenu(BuildContext context) {
  return PopupMenuButton<String>(
    onSelected: (value) => handleMenuItemSelection(context, value),
    itemBuilder: (context) => <PopupMenuEntry<String>>[
      PopupMenuItem<String>(
        value: settings,
        child: ListTile(
          leading: const Icon(Icons.settings),
          title: Text(AppLocalizations.of(context)!.settingsMenuItem),
        ),
      ),
    ],
    tooltip: AppLocalizations.of(context)!.systemMenuTooltip,
    child: Text(AppLocalizations.of(context)!.systemMenu),
  );

  /*return MouseRegion(
      onHover: (event) => mpm.onSelected!('Test'),
      child: SizedBox(child: mpm, width: 20, height: 20));*/
}
