// Flutter imports:
// ignore_for_file: use_build_context_synchronously

import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:nextcloud_password_client/constants/api_constants.dart';
import 'package:nextcloud_password_client/view_models/tag_list_view_model.dart';
import 'package:nextcloud_password_client/widgets/dialogues/folder_creation_dialogue.dart';
import 'package:nextcloud_password_client/widgets/dialogues/folder_editing_dialogue.dart';
import 'package:nextcloud_password_client/widgets/dialogues/password_creation_dialogue.dart';
import 'package:nextcloud_password_client/widgets/dialogues/password_editing_dialogue.dart';
import 'package:nextcloud_password_client/widgets/dialogues/tag_creation_dialogue.dart';
import 'package:nextcloud_password_client/widgets/dialogues/tag_editing_dialogue.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/menu_constants.dart';
import 'package:nextcloud_password_client/utils/http_utils.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/folder_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_list_view_model.dart';

Future<void> handleMenuItemSelection(BuildContext context, String value) async {
  switch (value) {
    case settings:
      context.read<ViewState>().showSettings();
      break;

    case onlineHelp:
      HttpUtils().openUrl(
          'https://gitlab.com/j0chn/nextcloud-password-client/-/wikis/home');
      break;

    case donate:
      HttpUtils().openUrl('https://www.ko-fi.com/j0chn');
      break;

    case reportBug:
      HttpUtils().openUrl(
          'https://gitlab.com/j0chn/nextcloud-password-client/-/issues');
      break;

    case editPassword:
      await PasswordEditingDialogue.showPasswordEditDialogue(context);
      break;

    case deletePassword:
      PasswordListViewModel passwordListViewModel =
          context.read<PasswordListViewModel>();
      passwordListViewModel.delete(context,
          passwordId: context.read<ViewState>().selectedPassword);
      break;

    case createFolder:
      await FolderCreationDialogue.showFolderCreationDialogue(context);
      break;

    case editFolder:
      String folderId = context.read<ViewState>().selectedFolder;

      if (folderId == apiRootFolder) {
        showToast(AppLocalizations.of(context)!.folderNotEditable,
            context: context);
      } else {
        await FolderEditingDialogue.showFolderEditingDialogue(
            context, folderId);
      }

      break;

    case deleteFolder:
      String folderId = context.read<ViewState>().selectedFolder;
      if (folderId == apiRootFolder) {
        showToast(AppLocalizations.of(context)!.folderNotDeletable,
            context: context);
      } else {
        FolderListViewModel folderListViewModel =
            context.read<FolderListViewModel>();

        folderListViewModel.delete(context, folderId: folderId);
      }

      break;

    case deleteTag:
      TagListViewModel tagListViewModel = context.read<TagListViewModel>();

      tagListViewModel.deleteTag(tagId: context.read<ViewState>().selectedTag);

      break;

    case editTag:
      TagEditingDialogue.showTagEditingDialogue(
          context, context.read<ViewState>().selectedTag);
      break;

    case createTag:
      TagCreationDialogue.showTagCreationDialogue(context);
      break;

    case createPassword:
      PasswordCreationDialogue.showPasswordCreationDialogue(context);
      break;

    default:
      showToast(AppLocalizations.of(context)!.notYetImplemented,
          context: context);
  }
}
