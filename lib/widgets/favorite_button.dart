// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:nextcloud_password_client/models/base_model.dart';

class FavoriteIcon extends StatefulWidget {
  final BaseModel baseModel;
  const FavoriteIcon(this.baseModel, {super.key});

  @override
  State<StatefulWidget> createState() => _FavoriteIconState();
}

class _FavoriteIconState extends State<FavoriteIcon> {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      iconSize: 20,
      splashRadius: 20,
      onPressed: () {
        widget.baseModel.favorite = !widget.baseModel.favorite;
        widget.baseModel.setFavorite(context, widget.baseModel.favorite);

        setState(() {});
      },
      icon: widget.baseModel.favorite
          ? const Icon(Icons.star, color: Colors.yellow)
          : const Icon(Icons.star_border, color: Colors.yellow),
    );
  }
}
