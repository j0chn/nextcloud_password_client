// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/controller/textfield_controller.dart';
import 'package:nextcloud_password_client/provider/nextcloud_provider.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/credentials_view_model.dart';

Widget continueButton(BuildContext context) {
  ViewState viewState = context.watch<ViewState>();

  bool enableContinueButton =
      viewState.isURLReachable && viewState.credentialsProvided;

  return viewState.isLoading
      ? const CircularProgressIndicator()
      : TextButton(
          style: ButtonStyle(
            foregroundColor: enableContinueButton
                ? MaterialStateProperty.all(Colors.white)
                : MaterialStateProperty.all(Colors.black),
            backgroundColor: enableContinueButton
                ? MaterialStateProperty.all(Colors.blue)
                : MaterialStateProperty.all(Colors.grey),
          ),
          onPressed: enableContinueButton ? () => _continue(context) : null,
          child: Text(AppLocalizations.of(context)!.continueText),
        );
}

void _continue(BuildContext context) async {
  CredentialsViewModel credentialsViewModel =
      context.read<CredentialsViewModel>();

  credentialsViewModel.userName =
      TextFieldController.userNameFieldController.text;

  credentialsViewModel.password =
      TextFieldController.passwordFieldController.text;

  credentialsViewModel.clientPassword =
      TextFieldController.clientPasswordFieldController.text;

  await NextcloudProvider.logIntoNextcloud(context, true);
}
