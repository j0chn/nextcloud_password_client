// Flutter imports:
// ignore_for_file: use_build_context_synchronously

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/controller/textfield_controller.dart';
import 'package:nextcloud_password_client/provider/nextcloud_provider.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/credentials_view_model.dart';
import 'package:nextcloud_password_client/widgets/credential_input.dart';
import 'package:nextcloud_password_client/widgets/single_elements/label.dart';

class CredentialDialogue {
  static Future<void> showCredentialsDialogue(BuildContext context) async {
    context.read<ViewState>().hideData = true;

    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Label(AppLocalizations.of(context)!.enterCredentials),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Column(children: <Widget>[
                  credentialInput(context),
                  Row(
                    children: <Widget>[
                      Label(AppLocalizations.of(context)!
                          .ignoreMissingCredentials),
                      _IgnoreMissingCredentialsCheckBox(),
                    ],
                  )
                ]),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(AppLocalizations.of(context)!.accept),
              onPressed: () {
                _handleDialogButtonPress(context);
              },
            ),
          ],
        );
      },
    );
  }
}

void _handleDialogButtonPress(BuildContext context) async {
  CredentialsViewModel credentialsViewModel =
      context.read<CredentialsViewModel>();
  ConfigViewModel configViewModel = context.read<ConfigViewModel>();
  configViewModel.persistModel();
  ViewState viewState = context.read<ViewState>();
  String userName = TextFieldController.userNameFieldController.text;
  String password = TextFieldController.passwordFieldController.text;
  credentialsViewModel.userName = userName;
  credentialsViewModel.password = password;
  viewState.ignoreMissingCredentials ||
          (userName.isNotEmpty && password.isNotEmpty)
      ? viewState.relogin = false
      : viewState.relogin = true;
  if (userName.isNotEmpty && password.isNotEmpty) {
    await NextcloudProvider.logIntoNextcloud(context, true);
    viewState = context.read<ViewState>();
    if (viewState.credentialInputError || viewState.masterPasswordInputError) {
      return;
    }
  }
  credentialsViewModel.persistModel(
      configViewModel.saveCredentials, configViewModel.saveMasterPassword);
  context.read<ViewState>().hideData = false;
  Navigator.of(context).pop();
}

class _IgnoreMissingCredentialsCheckBox extends StatefulWidget {
  @override
  _IgnoreMissingCredentialsCheckBoxState createState() =>
      _IgnoreMissingCredentialsCheckBoxState();
}

class _IgnoreMissingCredentialsCheckBoxState
    extends State<_IgnoreMissingCredentialsCheckBox> {
  @override
  Widget build(BuildContext context) {
    ViewState viewState = context.read<ViewState>();
    bool ignoreMissingCredentials = viewState.ignoreMissingCredentials;
    return Checkbox(
      checkColor: Colors.white,
      value: ignoreMissingCredentials,
      onChanged: (value) {
        viewState.ignoreMissingCredentials = !ignoreMissingCredentials;
      },
    );
  }
}
