// Flutter imports:
// ignore_for_file: use_build_context_synchronously

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nextcloud_password_client/controller/textfield_controller.dart';
import 'package:nextcloud_password_client/utils/key_chain.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/folder_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/folder_view_model.dart';
import 'package:nextcloud_password_client/widgets/dropdowns/folder_selection_dropdown.dart';
import 'package:nextcloud_password_client/widgets/single_elements/general_text_input.dart';

// Project imports:
import 'package:nextcloud_password_client/widgets/single_elements/label.dart';
import 'package:provider/provider.dart';

class FolderCreationDialogue {
  static Future<void> showFolderCreationDialogue(BuildContext context) async {
    var folderSelectionDropdown = FolderSelectionDropDown();

    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Label(AppLocalizations.of(context)!.createFolder),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Column(children: <Widget>[
                  Row(
                    children: <Widget>[
                      Label(AppLocalizations.of(context)!.folderName),
                      SizedBox(
                          width: 400,
                          height: 70,
                          child: GeneralTextInput(
                              TextFieldController.folderNameController))
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Label(AppLocalizations.of(context)!.subfolder),
                      folderSelectionDropdown,
                    ],
                  )
                ]),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(AppLocalizations.of(context)!.cancel),
              onPressed: () {
                _closePopUp(context);
              },
            ),
            TextButton(
              child: Text(AppLocalizations.of(context)!.create),
              onPressed: () {
                _createFolder(
                    context,
                    TextFieldController.folderNameController.text,
                    folderSelectionDropdown.folder.id);
              },
            ),
          ],
        );
      },
    );
  }
}

void _closePopUp(BuildContext context) {
  TextFieldController.folderNameController.clear();
  Navigator.of(context).pop();
}

void _createFolder(
    BuildContext context, String folderName, String parentFolder) async {
  FolderViewModel newFolder = FolderViewModel();
  KeyChain keyChain = context.read<ConfigViewModel>().keyChain;
  newFolder.cseKey = keyChain.current;
  newFolder.cseType = keyChain.type;
  newFolder.label =
      keyChain.encrypt(folderName, newFolder.cseType, newFolder.cseKey);
  newFolder.parent = parentFolder;
  context.read<FolderListViewModel>().createFolder(context, newFolder);
  _closePopUp(context);
}
