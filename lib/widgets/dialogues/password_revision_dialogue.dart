// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nextcloud_password_client/view_models/password_list_view_model.dart';
import 'package:provider/provider.dart';
import 'package:simple_markdown_editor/simple_markdown_editor.dart';

// Project imports:
import 'package:nextcloud_password_client/controller/textfield_controller.dart';
import 'package:nextcloud_password_client/utils/key_chain.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_view_model.dart';
import 'package:nextcloud_password_client/widgets/single_elements/label.dart';

class PasswordRevisionDialouge {
  static Future<void> showPasswordRevisionDialogue(
      BuildContext context, PasswordViewModel passwordRevision) async {
    context.read<ViewState>().inEditMode = false;
    KeyChain keyChain = context.read<ConfigViewModel>().keyChain;

    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          scrollable: true,
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Label(AppLocalizations.of(context)!.passwordColumn),
                        SizedBox(
                          width: 400,
                          height: 70,
                          child: Label(keyChain.decrypt(passwordRevision.cseKey,
                              passwordRevision.password)),
                        ),
                        const SizedBox(
                          width: 40,
                          height: 70,
                        ),
                        Label(AppLocalizations.of(context)!.usernameColumn),
                        SizedBox(
                          width: 400,
                          height: 70,
                          child: Label(keyChain.decrypt(passwordRevision.cseKey,
                              passwordRevision.username)),
                        ),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Label(AppLocalizations.of(context)!.nameColumn),
                        SizedBox(
                          width: 400,
                          height: 70,
                          child: Label(keyChain.decrypt(
                              passwordRevision.cseKey, passwordRevision.label)),
                        ),
                        Label(AppLocalizations.of(context)!.urlColumn),
                        SizedBox(
                          width: 400,
                          height: 70,
                          child: Label(keyChain.decrypt(
                              passwordRevision.cseKey, passwordRevision.url)),
                        ),
                        Label(AppLocalizations.of(context)!.folder),
                        SizedBox(
                          width: 400,
                          height: 70,
                          child: TextButton(
                            onPressed: () {},
                            child: Label(
                              keyChain.decrypt(passwordRevision.cseKey,
                                  passwordRevision.label),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        SafeArea(
                          child: SizedBox(
                            width: 800,
                            height: 400,
                            child: Container(
                              height: 300,
                              decoration: BoxDecoration(
                                border: Border.all(color: Colors.black),
                                borderRadius: BorderRadius.circular(20),
                              ),
                              child: MarkdownFormField(
                                controller:
                                    TextFieldController.notesFieldController,
                                enableToolBar: true,
                                emojiConvert: true,
                                autoCloseAfterSelectEmoji: false,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(AppLocalizations.of(context)!.cancel),
              onPressed: () {
                _cancel(context);
              },
            ),
            TextButton(
              child: Text(AppLocalizations.of(context)!.restore),
              onPressed: () {
                _setPasswordReveision(context, passwordRevision.id);
              },
            )
          ],
        );
      },
    );
  }
}

void _setPasswordReveision(BuildContext context, String revisionID) {
  PasswordViewModel passwordViewModel = context
      .read<PasswordListViewModel>()
      .getPasswordById(context.read<ViewState>().selectedPassword);

  passwordViewModel.restorePassword(context, revisionID);

  Navigator.of(context).pop();
}

void _cancel(BuildContext context) {
  Navigator.of(context).pop();
}
