// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nextcloud_password_client/widgets/single_elements/label.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/controller/textfield_controller.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/credentials_view_model.dart';
import 'package:nextcloud_password_client/widgets/single_elements/password_input.dart';

class ClientPasswordDialogue {
  static Future<void> showClientPasswordDialogue(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Label(AppLocalizations.of(context)!.enterClientPassword),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    PasswordText(
                        TextFieldController.clientPasswordFieldController,
                        labelText:
                            AppLocalizations.of(context)!.enterClientPassword,
                        errorText:
                            context.read<ViewState>().clientPasswordInputError
                                ? AppLocalizations.of(context)!
                                    .clientPasswordException
                                : ''),
                  ],
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(AppLocalizations.of(context)!.accept),
              onPressed: () {
                _accept(context);
              },
            ),
          ],
        );
      },
    );
  }
}

void _accept(BuildContext context) {
  ViewState viewState = context.read<ViewState>();

  if (context.read<CredentialsViewModel>().clientPassword ==
      TextFieldController.clientPasswordFieldController.text) {
    viewState.clientPasswordInputError = false;
    viewState.clientAuthenticated = true;
    Navigator.of(context).pop();
  } else {
    viewState.clientPasswordInputError = true;
    viewState.clientAuthenticated = false;
  }
}
