// Project imports:
import 'package:nextcloud_password_client/exceptions/general_exception.dart';

class DecryptException extends GeneralException {
  DecryptException(super.msg);

  @override
  String toString() {
    return msg;
  }
}
