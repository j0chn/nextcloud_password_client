// Dart imports:
import 'dart:async';
import 'dart:convert';

// Package imports:
import 'package:http/http.dart' as http;

// Project imports:
import 'package:nextcloud_password_client/constants/api_constants.dart';
import 'package:nextcloud_password_client/utils/http_utils.dart';
import 'package:nextcloud_password_client/view_models/folder_view_model.dart';

class NextcloudFolderProvider {
  static Future<Map<String, List<FolderViewModel>>> retrieveFolders() async {

    http.Response requestSessionResponse = await HttpUtils().httpPost(
      apiGetFolderList,
      body: jsonEncode({'details': apiGetFolderListDetail})
    );
        
    var body = json.decode(requestSessionResponse.body);

    Map<String, List<FolderViewModel>> models = {};

    for (Map<String, dynamic> folder in body) {
      
      FolderViewModel fvm = FolderViewModel.fromMap(folder);

      if (!models.containsKey(fvm.parent)) {
        
        models[fvm.parent] = [];

      }

      models[fvm.parent]!.add(fvm);

    }

    return models;

  }

  static Future<bool> deleteFolder(String folderBody) async{

    http.Response response = await HttpUtils().httpDelete(
      apiPostFolderDelete,
      body: folderBody
    );

    return response.statusCode == 200;
  }

static Future<FolderViewModel>createFolder(String folderBody) async{
  
    FolderViewModel folderViewModel = FolderViewModel();

    http.Response response = await HttpUtils().httpPost(
      apiPostFolderCreate, 
      body: folderBody
    );

    if (response.statusCode == 201) {

      var body = json.decode(response.body);

      folderViewModel = FolderViewModel.fromMap(body);

    }

    return folderViewModel;

  }

static Future<bool> updateFolder(String folderBody) async{

    http.Response response = await HttpUtils().httpUpdate(
      apiPatchFolderUpdate,
      body: folderBody
    );

    return response.statusCode == 200;

  }

  static Future<FolderViewModel>setFolderToFavorite(String folderBody) async{

    FolderViewModel folderViewModel = FolderViewModel();

    http.Response response = await HttpUtils().httpUpdate(
      apiPatchFolderUpdate, 
      body: folderBody
    );

    if (response.statusCode == 200) {

      var body = json.decode(response.body);

      folderViewModel = FolderViewModel.fromMap(body);

    }

    return folderViewModel;
    
  }

}
