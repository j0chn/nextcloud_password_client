// Project imports:
import 'package:nextcloud_password_client/models/user_model.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';

class UserViewModel extends UserModel {
  DataAccessLayer dataAccessLayer = DataAccessLayer();
  UserModel _userModel = UserModel();
  UserModel get userModel => _userModel;

  @override
  UserViewModel({DataAccessLayer? dataAccessLayer})
      : dataAccessLayer = dataAccessLayer ?? DataAccessLayer();

  UserViewModel.fromModel(UserModel model) {
    _userModel = UserModel();
  }

  UserViewModel.fromMap(String userId, String userName) {
    _userModel = UserModel();
    _userModel.id = id = userId;
    _userModel.displayName = displayName = userName;
  }

  void refreshModel(
      {required UserModel fromModel, required UserModel toModel}) {
    toModel.id = fromModel.id;
    toModel.displayName = fromModel.displayName;

    notifyListeners();
  }

  void refreshViewModel(UserModel userModel) {
    refreshModel(fromModel: userModel, toModel: this);
  }

  void refreshDataModel(UserViewModel userViewModel) {
    refreshModel(fromModel: userViewModel, toModel: _userModel);
  }
}
