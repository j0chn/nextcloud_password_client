// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'config_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ConfigModelAdapter extends TypeAdapter<ConfigModel> {
  @override
  final int typeId = 0;

  @override
  ConfigModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return ConfigModel()
      ..serverURL = fields[0] as String
      ..$useLocalCopy = fields[1] as bool
      ..masterPasswordRequired = fields[2] as bool
      ..$saveCredentials = fields[3] as bool
      ..$saveMasterPassword = fields[4] as bool
      ..loginSucceeded = fields[5] as bool
      ..session = fields[6] as String
      ..keyChain = fields[7] as KeyChain
      ..$selectedTheme = fields[8] as Themes
      ..$refreshRateInSeconds = fields[9] as int
      ..$temporaryID = fields[10] as int;
  }

  @override
  void write(BinaryWriter writer, ConfigModel obj) {
    writer
      ..writeByte(11)
      ..writeByte(0)
      ..write(obj.serverURL)
      ..writeByte(1)
      ..write(obj.$useLocalCopy)
      ..writeByte(2)
      ..write(obj.masterPasswordRequired)
      ..writeByte(3)
      ..write(obj.$saveCredentials)
      ..writeByte(4)
      ..write(obj.$saveMasterPassword)
      ..writeByte(5)
      ..write(obj.loginSucceeded)
      ..writeByte(6)
      ..write(obj.session)
      ..writeByte(7)
      ..write(obj.keyChain)
      ..writeByte(8)
      ..write(obj.$selectedTheme)
      ..writeByte(9)
      ..write(obj.$refreshRateInSeconds)
      ..writeByte(10)
      ..write(obj.$temporaryID);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ConfigModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
