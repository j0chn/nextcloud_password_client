// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'credentials_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CredentialsModelAdapter extends TypeAdapter<CredentialsModel> {
  @override
  final int typeId = 5;

  @override
  CredentialsModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CredentialsModel()
      ..userName = fields[0] as String
      ..password = fields[1] as String
      ..masterPassword = fields[2] as String
      ..clientPassword = fields[3] as String;
  }

  @override
  void write(BinaryWriter writer, CredentialsModel obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.userName)
      ..writeByte(1)
      ..write(obj.password)
      ..writeByte(2)
      ..write(obj.masterPassword)
      ..writeByte(3)
      ..write(obj.clientPassword);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CredentialsModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
