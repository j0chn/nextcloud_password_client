// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'password_share_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PasswordShareModelAdapter extends TypeAdapter<PasswordShareModel> {
  @override
  final int typeId = 8;

  @override
  PasswordShareModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return PasswordShareModel()
      ..id = fields[0] as String
      ..created = fields[1] as int
      ..updated = fields[2] as int
      ..expires = fields[3] as int
      ..editable = fields[4] as bool
      ..shareable = fields[5] as bool
      ..updatePending = fields[6] as bool
      ..password = fields[7] as String
      ..owner = fields[8] as dynamic
      ..receiver = fields[9] as dynamic;
  }

  @override
  void write(BinaryWriter writer, PasswordShareModel obj) {
    writer
      ..writeByte(10)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.created)
      ..writeByte(2)
      ..write(obj.updated)
      ..writeByte(3)
      ..write(obj.expires)
      ..writeByte(4)
      ..write(obj.editable)
      ..writeByte(5)
      ..write(obj.shareable)
      ..writeByte(6)
      ..write(obj.updatePending)
      ..writeByte(7)
      ..write(obj.password)
      ..writeByte(8)
      ..write(obj.owner)
      ..writeByte(9)
      ..write(obj.receiver);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PasswordShareModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
