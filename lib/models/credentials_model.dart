// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:equatable/equatable.dart';
import 'package:hive_flutter/hive_flutter.dart';

part 'credentials_model.g.dart';

@HiveType(typeId: 5)
class CredentialsModel extends ChangeNotifier with EquatableMixin {

  ///logon user name for nextcloud itself
  @HiveField(0)
  String userName = '';
  
  ///logon password for nextcloud itself
  @HiveField(1)
  String password = '';
  
  ///logon master password for the passwords app (if set)
  @HiveField(2)
  String masterPassword = '';
  
  ///password to protect this app (if set)
  @HiveField(3)
  String clientPassword = '';

  CredentialsModel();

  @override
  List<Object?> get props =>
      [
        userName, 
        password, 
        masterPassword, 
        clientPassword
      ];
}
