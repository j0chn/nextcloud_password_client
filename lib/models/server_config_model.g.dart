// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'server_config_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ServerConfigModelAdapter extends TypeAdapter<ServerConfigModel> {
  @override
  final int typeId = 7;

  @override
  ServerConfigModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return ServerConfigModel()
      ..userPasswordGeneratorStrength = fields[0] as int
      ..userPasswordGeneratorNumbers = fields[1] as bool
      ..userPasswordGeneratorSpecial = fields[2] as bool
      ..usePasswordSecurityDuplicates = fields[3] as bool
      ..usePasswordSecurityAge = fields[4] as int
      ..userPasswordSecurityHash = fields[5] as int
      ..useMailSecurity = fields[6] as bool
      ..userMailShares = fields[7] as bool
      ..userNotificationSecurity = fields[8] as bool
      ..userNotificationShares = fields[9] as bool
      ..userNotificationErrors = fields[10] as bool
      ..userNotificationAdmin = fields[11] as bool
      ..userEncryptionSse = fields[12] as int
      ..userEncryptionCse = fields[13] as int
      ..userSharingEditable = fields[14] as bool
      ..userSharingResharing = fields[15] as bool
      ..userSessionLifetime = fields[16] as int
      ..serverVersion = fields[17] as String
      ..serverAppVersion = fields[18] as String
      ..serverBaseUrl = fields[19] as String
      ..serverBaseUrlWebdav = fields[20] as String
      ..serverSharingEnabled = fields[21] as bool
      ..serverSharingResharing = fields[22] as bool
      ..serverSharingAutocomplete = fields[23] as bool
      ..serverSharingTypes = (fields[24] as List).cast<String>()
      ..serverThemeColorPrimary = fields[25] as String
      ..serverThemeColorText = fields[26] as String
      ..serverThemeColorBackground = fields[27] as String
      ..serverThemeBackground = fields[28] as String
      ..serverThemeLogo = fields[29] as String
      ..serverThemeLabel = fields[30] as String
      ..serverThemeAppIcon = fields[31] as String
      ..serverThemeFolderIcon = fields[32] as String
      ..serverHandbookUrl = fields[33] as String
      ..serverPerformance = fields[34] as int;
  }

  @override
  void write(BinaryWriter writer, ServerConfigModel obj) {
    writer
      ..writeByte(35)
      ..writeByte(0)
      ..write(obj.userPasswordGeneratorStrength)
      ..writeByte(1)
      ..write(obj.userPasswordGeneratorNumbers)
      ..writeByte(2)
      ..write(obj.userPasswordGeneratorSpecial)
      ..writeByte(3)
      ..write(obj.usePasswordSecurityDuplicates)
      ..writeByte(4)
      ..write(obj.usePasswordSecurityAge)
      ..writeByte(5)
      ..write(obj.userPasswordSecurityHash)
      ..writeByte(6)
      ..write(obj.useMailSecurity)
      ..writeByte(7)
      ..write(obj.userMailShares)
      ..writeByte(8)
      ..write(obj.userNotificationSecurity)
      ..writeByte(9)
      ..write(obj.userNotificationShares)
      ..writeByte(10)
      ..write(obj.userNotificationErrors)
      ..writeByte(11)
      ..write(obj.userNotificationAdmin)
      ..writeByte(12)
      ..write(obj.userEncryptionSse)
      ..writeByte(13)
      ..write(obj.userEncryptionCse)
      ..writeByte(14)
      ..write(obj.userSharingEditable)
      ..writeByte(15)
      ..write(obj.userSharingResharing)
      ..writeByte(16)
      ..write(obj.userSessionLifetime)
      ..writeByte(17)
      ..write(obj.serverVersion)
      ..writeByte(18)
      ..write(obj.serverAppVersion)
      ..writeByte(19)
      ..write(obj.serverBaseUrl)
      ..writeByte(20)
      ..write(obj.serverBaseUrlWebdav)
      ..writeByte(21)
      ..write(obj.serverSharingEnabled)
      ..writeByte(22)
      ..write(obj.serverSharingResharing)
      ..writeByte(23)
      ..write(obj.serverSharingAutocomplete)
      ..writeByte(24)
      ..write(obj.serverSharingTypes)
      ..writeByte(25)
      ..write(obj.serverThemeColorPrimary)
      ..writeByte(26)
      ..write(obj.serverThemeColorText)
      ..writeByte(27)
      ..write(obj.serverThemeColorBackground)
      ..writeByte(28)
      ..write(obj.serverThemeBackground)
      ..writeByte(29)
      ..write(obj.serverThemeLogo)
      ..writeByte(30)
      ..write(obj.serverThemeLabel)
      ..writeByte(31)
      ..write(obj.serverThemeAppIcon)
      ..writeByte(32)
      ..write(obj.serverThemeFolderIcon)
      ..writeByte(33)
      ..write(obj.serverHandbookUrl)
      ..writeByte(34)
      ..write(obj.serverPerformance);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ServerConfigModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
