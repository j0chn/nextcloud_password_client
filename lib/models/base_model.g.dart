// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'base_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class BaseModelAdapter extends TypeAdapter<BaseModel> {
  @override
  final int typeId = 13;

  @override
  BaseModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return BaseModel()
      ..id = fields[0] as String
      ..label = fields[1] as String
      ..revision = fields[2] as String
      ..cseType = fields[3] as String
      ..cseKey = fields[4] as String
      ..sseType = fields[5] as String
      ..client = fields[6] as String
      ..hidden = fields[7] as bool
      ..trashed = fields[8] as bool
      ..favorite = fields[9] as bool
      ..created = fields[10] as int
      ..updated = fields[11] as int
      ..edited = fields[12] as int;
  }

  @override
  void write(BinaryWriter writer, BaseModel obj) {
    writer
      ..writeByte(13)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.label)
      ..writeByte(2)
      ..write(obj.revision)
      ..writeByte(3)
      ..write(obj.cseType)
      ..writeByte(4)
      ..write(obj.cseKey)
      ..writeByte(5)
      ..write(obj.sseType)
      ..writeByte(6)
      ..write(obj.client)
      ..writeByte(7)
      ..write(obj.hidden)
      ..writeByte(8)
      ..write(obj.trashed)
      ..writeByte(9)
      ..write(obj.favorite)
      ..writeByte(10)
      ..write(obj.created)
      ..writeByte(11)
      ..write(obj.updated)
      ..writeByte(12)
      ..write(obj.edited);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BaseModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
