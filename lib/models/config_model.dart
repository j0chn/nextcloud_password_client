// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:equatable/equatable.dart';
import 'package:hive_flutter/hive_flutter.dart';

// Project imports:
import 'package:nextcloud_password_client/enums/themes.dart';
import 'package:nextcloud_password_client/utils/key_chain.dart';

part 'config_model.g.dart';

@HiveType(typeId: 0)
class ConfigModel extends ChangeNotifier with EquatableMixin {
  ///URL of the nextcloud instance to connect to
  @HiveField(0)
  String serverURL = '';

  ///flag if the data should be saved locally
  @HiveField(1)
  bool $useLocalCopy = false;

  ///flag if the data should be saved locally
  @HiveField(2)
  bool masterPasswordRequired = false;

  ///flag if login data should be saved locally
  @HiveField(3)
  bool $saveCredentials = false;
  
  ///flag if the server data should be saved locally
  @HiveField(4)
  bool $saveMasterPassword = false;
  
  ///flag if the login was successful
  @HiveField(5)
  bool loginSucceeded = false;
  
  ///session ID to keep the session alive
  @HiveField(6)
  String session = '';

  ///keyyChain to decrypt and encrypt the data
  @HiveField(7)
  KeyChain keyChain = KeyChain.none();
  
  ///the apps color sheme, can be changed in settings
  @HiveField(8)
  Themes $selectedTheme = Themes.light;
  
  ///interval to retrieve data from server
  @HiveField(9)
  int $refreshRateInSeconds = 10;

  ///this is used for new local objecs until they get replaced by the server 
  @HiveField(10)
  int $temporaryID = 0;

  ConfigModel();

  bool get useLocalCopy => $useLocalCopy;

  bool get saveCredentials => $saveCredentials;

  bool get saveMasterPassword => $saveMasterPassword;

  Themes get selectedTheme => $selectedTheme;

  @override
  List<Object?> get props => [
        serverURL,
        $useLocalCopy,
        masterPasswordRequired,
        $saveCredentials,
        $saveMasterPassword,
        loginSucceeded,
        session,
        $selectedTheme,
        $refreshRateInSeconds,
        $temporaryID,
      ];
}
