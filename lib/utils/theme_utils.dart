// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:nextcloud_password_client/enums/themes.dart';
import 'package:nextcloud_password_client/models/server_config_model.dart';
import 'package:nextcloud_password_client/themes/nextcloud_theme.dart';

class ThemeUtils {

  static ThemeData getTheme(
    Themes theme,
    {ServerConfigModel? serverConfigModel}) {
        
    switch (theme) {

      case Themes.dark:
      
        return ThemeData.dark();
      
      case Themes.custom:
      
        return CustomThemes.customTheme;
      
      case Themes.nextcloud:
  
        serverConfigModel ??= ServerConfigModel();
    
        return CustomThemes.getNextcloudTheme(serverConfigModel);
      
      case Themes.light:
      
      default:
      
        return ThemeData.light();
    }

  }

}
