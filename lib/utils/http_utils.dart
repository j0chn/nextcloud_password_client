// Dart imports:
import 'dart:convert';
import 'dart:io';

// Package imports:
import 'package:http/http.dart' as http;
import 'package:http/io_client.dart';
import 'package:url_launcher/url_launcher_string.dart';

class HttpUtils {

  static final HttpUtils _httpUtils = HttpUtils._internal();
  int? _status = 0;
  bool? _result = false;
  bool? _isUrlValid = false;
  String _url = '';
  String _userName = '';
  String _password = '';
  String _session = '';

  factory HttpUtils() => _httpUtils;

  HttpUtils._internal();

  setLoginData(String url, String username, String password, String session) {

    _url = url;
    _userName = username;
    _password = password;
    _session = session;
  
  }

  static bool _certificateCheck(
    X509Certificate cert, 
    String host, 
    int port) => true;

  final IOClient _client = IOClient(HttpClient()
    ..badCertificateCallback = _certificateCheck
    ..maxConnectionsPerHost = 1);

  Future<bool?> checkConn(String url) async {
  
    if (url.isNotEmpty) {
  
      await _client.get(Uri.parse(url)).then((response) async {
  
        _status = response.statusCode;
  
        if (_status == null) {
  
          _result = false;
  
          return false;
  
        } else {
  
          _result = _status == 200;
          _status = null;
  
          return _result;
  
        }
  
      }).onError((dynamic error, stackTrace) {
        
        _status = null;
        _result = null;
        
        return _result;
      
      });
    
    }
    
    if (_status == null && (_result == false || _result == null)) {
    
      _isUrlValid = false;
      return false;
    
    } else {
    
      _isUrlValid = _result;
      _status = null;
      _result = null;
      return _isUrlValid;
    
    }
  
  }

  Future<http.Response> httpGet(String uri) async {
  
    return _client.get(
      Uri.parse(_url + uri), 
      headers: _getHeaders()
    );
  
  }

  Future<http.Response> httpPost(String uri, {String? body}) async {
  
    return _client.post(
      Uri.parse(_url + uri),
      headers: _getHeaders(), 
      body: body
    );
  
  }

  Future<http.Response> httpDelete(String uri, {String? body}) async {
  
    return _client.delete(
      Uri.parse(_url + uri),
      headers: _getHeaders(), 
      body: body
    );
  
  }

  Future<http.Response> httpUpdate(String uri, {String? body}) async {
    
    return _client.patch(
      Uri.parse(_url + uri),
      headers: _getHeaders(), 
      body: body
    );
  
  }

  Map<String, String> _getHeaders({bool contentType = true}) {
  
    final basicAuth =
        'Basic ${base64Encode(utf8.encode('$_userName:$_password'))}';
  
    Map<String, String> headers = {
      'OCS-APIRequest': 'true',
      'authorization': basicAuth,
      if (_session != '') 'x-api-session': _session,
      if (contentType) 'Content-Type': 'application/json',
      if (contentType) 'accept': 'application/json'
  
    };
  
    return headers;
  
  }

  void openUrl(String url) async {

    await canLaunchUrlString(url)
        ? await launchUrlString(url)
        : throw 'Could not launch $url';
  
  }

  Future<http.Response> httpGetFavIcon(
      String uri, 
      String userName, 
      String password) async {
    
    String url = _url + uri;
    url = url.replaceFirst('https://', 'https://$userName:$password@');
    
    return _client.get(
      Uri.parse(url), 
      headers: _getHeaders()
    );
  
  }

}
