import 'dart:io';

import 'package:path_provider/path_provider.dart';

class Logger {

  static Future<void> logError(Object error, StackTrace stackTrace) async {
  
    final directory = await getApplicationSupportDirectory();

    final path = directory.path;

    final file = await File('$path/error.log').create(recursive: true);
  
    var sink = file.openWrite(mode: FileMode.append);
  
    sink.writeln('Unhandled Exception at ${DateTime.now().toString()}');
    sink.writeln('-----------------------------------------------------');
    sink.writeln(error.toString());
    sink.writeln(stackTrace.toString());
  
    await sink.flush();
    await sink.close();
  
  }

}
