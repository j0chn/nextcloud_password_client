// Flutter imports:
import 'package:bitsdojo_window_v3/bitsdojo_window_v3.dart';
import 'package:flutter/material.dart';

// Package imports:
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/widget_constants.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/widgets/menu/menu_row.dart';

const borderColor = Color(0xFF805306);

PreferredSize getWindowBorder(BuildContext context, bool createMenus) {

  return PreferredSize(
    preferredSize: const Size.fromHeight(35.0),
    child: 
    
    AppBar(
      flexibleSpace: WindowBorder(
        color: const Color(0xFF805306),
        width: 1,
        child: getWindow(context, createMenus),
      ),

    ),

  );

}

const sidebarColor = Color(0xFFF6A00C);

Widget getWindow(BuildContext context, bool createMenus) {
  return Column(children: [
    
    WindowTitleBarBox(
      child: 

      Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          
          if (createMenus) ...[getMenuBar(context)],
          Expanded(
            child: 

            MoveWindow(
              child: 
            
              Container(
                  alignment: Alignment.center, child: const Text(appTitle)),
            ),

          ),

          const WindowButtons()
        
        ],

      ),

    ),

  ]
  
  );
  
}

final buttonColors = WindowButtonColors(
    iconNormal: const Color(0xFF805306),
    mouseOver: const Color(0xFFF6A00C),
    mouseDown: const Color(0xFF805306),
    iconMouseOver: const Color(0xFF805306),
    iconMouseDown: const Color(0xFFFFD500));

final closeButtonColors = WindowButtonColors(
    mouseOver: const Color(0xFFD32F2F),
    mouseDown: const Color(0xFFB71C1C),
    iconNormal: const Color(0xFF805306),
    iconMouseOver: Colors.white);

class WindowButtons extends StatelessWidget {
  const WindowButtons({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        MinimizeWindowButton(colors: buttonColors),
        MaximizeWindowButton(colors: buttonColors),
        CloseWindowButton(
            colors: closeButtonColors,
            onPressed: () async => {
                  [
                    await context.read<ViewState>().handleAppClosing(),
                    appWindow.close()
                  ]
                }),
      ],
    );
  }
}
