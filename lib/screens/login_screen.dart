// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/screens/window_border.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/widgets/client_password.dart';
import 'package:nextcloud_password_client/widgets/continue_button.dart';
import 'package:nextcloud_password_client/widgets/credential_input.dart';
import 'package:nextcloud_password_client/widgets/local_copy.dart';
import 'package:nextcloud_password_client/widgets/server_connection.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: getWindowBorder(context, false),
      bottomNavigationBar: SizedBox(
        height: 50,
        width: 100,
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
          alignment: Alignment.bottomRight,
          child: BottomAppBar(
            color: Colors.transparent,
            elevation: 0,
            child: continueButton(context),
          ),
        ),
      ),
      body: Container(
        alignment: Alignment.center,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(width: 360, child: serverConnection(context)),
            if (context.watch<ViewState>().isURLReachable) ...[
              SizedBox(
                width: 300,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    credentialInput(context),
                    localCopyWidgets(context),
                    clientPassword(context),
                  ],
                ),
              ),
            ],
          ],
        ),
      ),
    );
  }
}
