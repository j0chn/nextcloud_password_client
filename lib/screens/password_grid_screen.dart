// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:pluto_grid_plus/pluto_grid_plus.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/password_list_view_model.dart';
import 'package:nextcloud_password_client/widgets/password_grid/password_column.dart';
import 'package:nextcloud_password_client/widgets/password_grid/password_row.dart';

Widget passwordGridScreen(BuildContext context) {

  bool obscurePassword = context.watch<ViewState>().obscurePassword;
  
  return context.watch<PasswordListViewModel>().passwordViewModels.isEmpty ||
          context.watch<ViewState>().hideData
      ? const CircularProgressIndicator()
      : PlutoGrid(
          onRowChecked: (event) => context.read<ViewState>().selectedPassword =
              event.row!.key.toString(),
          onSelected: (PlutoGridOnSelectedEvent event) {
            ValueKey<String> key = event.row!.key as ValueKey<String>;
            context.read<ViewState>().selectedPassword = key.value;
          },

          onLoaded: (PlutoGridOnLoadedEvent event) {

            ViewState viewState = context.read<ViewState>();
            //Nodes lastSelectedNode = viewState.selectedNode;
            if (viewState.sortedColumn.isNotEmpty) {
              
              PlutoColumn? sortColumn;
              
              for (PlutoColumn column in event.stateManager.columns) {
              
                if (column.title == viewState.sortedColumn) {
              
                  sortColumn = column;
              
                }
              
              }

              if (sortColumn != null) {
                
                switch (viewState.columnSort) {
                
                  case 'none':
                
                  case '':
                
                    break;
                
                  case 'ascending':
                
                    event.stateManager.sortAscending(sortColumn);
                
                    break;
                
                  case 'descending':
                
                    event.stateManager.sortDescending(sortColumn);
                
                    break;
                }

              }

            }

            event.stateManager.setSelectingMode(PlutoGridSelectingMode.none);

            PasswordListViewModel passwordListViewModel =
                context.read<PasswordListViewModel>();
                
            passwordListViewModel.plutoStateManager = event.stateManager;
            
            passwordListViewModel.setPasswordsInitially(context);
            
            PlutoRow? plutoRow = passwordListViewModel
                .getRowByPassword(context.read<ViewState>().selectedPassword);
            
            if (plutoRow != null) {
            
              event.stateManager.setCurrentCell(
                  plutoRow.cells.values.first, plutoRow.state.index);
            
            }
            
            event.stateManager.addListener(() {
              /*
              TODO: change logic so passwords are returned and the grid is reseted here not in passwordlistviewmodel.
              if (lastSelectedNode != context.read<ViewState>().selectedNode) {
                event.stateManager.appendRows(passwordListViewModel.get);
              }*/
              if (event.stateManager.getSortedColumn != null) {

                viewState.sortedColumn =
                    event.stateManager.getSortedColumn!.title;
                
                switch (event.stateManager.getSortedColumn?.sort) {
                
                  case PlutoColumnSort.none:
                
                  case null:
                
                    viewState.columnSort = 'none';
                
                    break;
                
                  case PlutoColumnSort.ascending:
                
                    viewState.columnSort = 'ascending';
                
                    break;
                
                  case PlutoColumnSort.descending:
                
                    viewState.columnSort = 'descending';
                
                    break;
                }

              }

            }
            
            );

          },

          columns: getPasswordHeader(context, obscurePassword),
          rows: getPasswordRows(context),
          mode: PlutoGridMode.select,
          /*configuration:  
              context.read<ConfigViewModel>().selectedTheme == Themes.dark
                  ? const PlutoGridConfiguration.dark()
                  : const PlutoGridConfiguration.dark(),*/
      );
}
