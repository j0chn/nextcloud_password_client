// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/controller/textfield_controller.dart';
import 'package:nextcloud_password_client/utils/http_utils.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_view_model.dart';

Widget getPasswordNotesTab(BuildContext context, PasswordViewModel password) {

  ConfigViewModel configViewModel = context.read<ConfigViewModel>();
  
  TextEditingController passwordController =
      TextFieldController.passwordController;
  
  passwordController.text =
      configViewModel.keyChain.decrypt(password.cseKey, password.password);
  
  final ScrollController detailScrollController = ScrollController();
  final ScrollController detailScrollController2 = ScrollController();
  
  return SingleChildScrollView(
  
    controller: detailScrollController,
    scrollDirection: Axis.vertical,
    child: 
    
    SingleChildScrollView(
      controller: detailScrollController2,
      scrollDirection: Axis.horizontal,
      child: 
      
      SingleChildScrollView(
        child: 
        
        MarkdownBody(
          onTapLink: (text, href, title) => HttpUtils().openUrl(href!),
          data: configViewModel.keyChain.decrypt(
                password.cseKey, 
                password.notes)
          ),
      ),

    ),

  );

}
