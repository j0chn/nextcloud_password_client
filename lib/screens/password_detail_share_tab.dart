// Flutter imports:
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

// Project imports:
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:nextcloud_password_client/provider/nextcloud_user_provider.dart';
import 'package:nextcloud_password_client/view_models/password_share_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_view_model.dart';
import 'package:nextcloud_password_client/view_models/user_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/user_view_model.dart';
import 'package:provider/provider.dart';

Widget getPasswordShareTab(BuildContext context, PasswordViewModel password) {

  String owner = '';
  
  if (password.passwordShareView.owner.toString().isNotEmpty) {
  
    owner = password.passwordShareView.owner['name'];
  
  }
  
  return Column(
  
    mainAxisSize: MainAxisSize.min,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
  
      Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: 
        
        [
          owner.toString().isNotEmpty
              ? SizedBox(
                  width: 400,
                  height: 30,
                  child: Text(password.passwordShareView.owner['name'] +
                      ' ' +
                      AppLocalizations.of(context)!.passwordSharedWithYou),
                )
              : const SizedBox.shrink(),
        ],

      ),

      SizedBox(
        width: 400,
        child: 

        TypeAheadField<UserViewModel>(
          suggestionsCallback: (search) => context.read<UserListViewModel>().userViewModels,
          builder: (context, controller, focusNode) {
            
            return TextField(
              controller: controller,
              focusNode: focusNode,
              autofocus: true,
              decoration: 
              
              InputDecoration(
                border: const OutlineInputBorder(),
                labelText: AppLocalizations.of(context)!.searchUser,
              )

            );

          },

          itemBuilder: (context, user) {
            
            return ListTile(
              title: Text(user.id),
              subtitle: Text(user.displayName),
            );

          },

          onSelected: (user) {
            
            NextcloudUserProvider.shareWithuser(password.id, user.id);
          },

        ),

      ),

      Expanded(
        child: 
        
        ListView.builder(
          itemCount: password.shareViewModels.length,
          itemBuilder: (BuildContext context, int index) {

            PasswordShareViewModel passwordShareViewModel =
                password.shareViewModels.elementAt(index);
            
            return ListTile(
              hoverColor: Colors.black12,
              onTap: () {},
              title: 
              
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: [
                  
                  Text(
                    passwordShareViewModel.receiver['name'],
                  ),

                  Expanded(
                    child: 
                    
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      mainAxisSize: MainAxisSize.min,
                      children: [

                        IconButton(
                            onPressed: () {passwordShareViewModel.updateEditable(); },
                            icon: Icon(Icons.create_rounded, color: passwordShareViewModel.editable ? Colors.green: Colors.red ),
                            tooltip: passwordShareViewModel.editable ? AppLocalizations.of(context)!.passwordShareDenyEditing : AppLocalizations.of(context)!.passwordShareAllowEditing,),
                        
                        IconButton(
                            onPressed: () {passwordShareViewModel.updateShareable();}, 
                            icon: Icon(Icons.share, color: passwordShareViewModel.shareable ? Colors.green: Colors.red ),
                            tooltip: passwordShareViewModel.editable ? AppLocalizations.of(context)!.passwordShareDenySharing : AppLocalizations.of(context)!.passwordShareAllowSharing),
                        
                        IconButton(
                            onPressed: () async {
                              bool expirationDateNotSet = passwordShareViewModel.expires == 000000000000000;
                              DateTime expirationDate = DateTime.fromMillisecondsSinceEpoch(passwordShareViewModel.expires * 1000);
                              DateTime? pickedDate = await showDatePicker(context: context, firstDate: expirationDateNotSet ? DateTime.now() : expirationDate,
                              lastDate: DateTime(9999,12,31),
                              initialDate: expirationDateNotSet ? DateTime.now() : expirationDate);
                              
                              if(pickedDate != null){
                                passwordShareViewModel.expires = pickedDate.millisecondsSinceEpoch * 1000;
                                passwordShareViewModel.updateExpiration();
                              }
                              },
                            icon: const Icon(Icons.calendar_today),
                            tooltip: AppLocalizations.of(context)!.passwordShareExpiration),
                        
                        IconButton(
                            onPressed: () {passwordShareViewModel.deleteShare();}, icon: const Icon(Icons.delete),
                            tooltip: AppLocalizations.of(context)!.deletePasswordShare)
                      ],

                    ),

                  ),

                ],

              ),

            );

          },

        ),

      ),

    ],

  );
  
}
