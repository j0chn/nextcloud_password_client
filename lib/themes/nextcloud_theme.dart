// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:nextcloud_password_client/models/server_config_model.dart';

class CustomThemes {

  static ThemeData getNextcloudTheme(ServerConfigModel serverConfigModel) {
  
    return ThemeData(
      brightness: Brightness.light,
      textTheme: 
      
      TextTheme(
        bodyLarge: 
        
        TextStyle(
              backgroundColor: Color(int.parse(serverConfigModel
                  .serverThemeColorBackground
                  .replaceFirst('#', '0xff'))),
              
              color: Color(int.parse(serverConfigModel.serverThemeColorText
                  .replaceFirst('#', '0xff'))))),
      
      primaryColor: Color(int.parse(
          serverConfigModel.serverThemeColorPrimary.replaceFirst('#', '0xff'))), 
          colorScheme: ColorScheme.fromSwatch(
      
            brightness: Brightness.light,
            primarySwatch: MaterialColor(
              int.parse(serverConfigModel.serverThemeColorPrimary
                  .replaceFirst('#', '0xff')),
              <int, Color>{
                50: Color(int.parse(serverConfigModel.serverThemeColorPrimary
                    .replaceFirst('#', '0xff'))),
                100: Color(int.parse(serverConfigModel.serverThemeColorPrimary
                    .replaceFirst('#', '0xff'))),
                200: Color(int.parse(serverConfigModel.serverThemeColorPrimary
                    .replaceFirst('#', '0xff'))),
                300: Color(int.parse(serverConfigModel.serverThemeColorPrimary
                    .replaceFirst('#', '0xff'))),
                400: Color(int.parse(serverConfigModel.serverThemeColorPrimary
                    .replaceFirst('#', '0xff'))),
                500: Color(int.parse(serverConfigModel.serverThemeColorPrimary
                    .replaceFirst('#', '0xff'))),
                600: Color(int.parse(serverConfigModel.serverThemeColorPrimary
                    .replaceFirst('#', '0xff'))),
                700: Color(int.parse(serverConfigModel.serverThemeColorPrimary
                    .replaceFirst('#', '0xff'))),
                800: Color(int.parse(serverConfigModel.serverThemeColorPrimary
                    .replaceFirst('#', '0xff'))),
                900: Color(int.parse(serverConfigModel.serverThemeColorPrimary
                    .replaceFirst('#', '0xff'))),
              },

            ),

      ).copyWith(
          background: 
          Color(int.parse(
            serverConfigModel
            .serverThemeColorBackground
            .replaceFirst('#', '0xff')
            )
          )
        ),
    );

  }

  static ThemeData get customTheme {

    return ThemeData(
      brightness: Brightness.dark,
      primaryColor: Colors.red,
      colorScheme: 
        ColorScheme.fromSwatch(
          brightness: Brightness.dark,
          primarySwatch: Colors.red,
        ),
      appBarTheme: const AppBarTheme(
        color: Colors.red,
      ),

    );

  }

}
