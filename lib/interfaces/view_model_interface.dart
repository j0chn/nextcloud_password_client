abstract class ViewModelInterface {
  Future<void> initialize();
  void refreshDataModels();
  void refreshViewModels();
  void persistModel([bool firstBool = false, bool secondBool = false]);
  void loadModel();
  void handleLogout();
}
