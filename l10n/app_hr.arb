{
  "createLocalCopy": "spremiti lozinke lokalno?",
  "@createLocalCopy": {
    "description": "Check if a local copy shall be created (as fallback in case the server is not reachable)."
  },
  "serverReached": "poslužitelj je dostupan.",
  "@serverReached": {
    "description": "status message when the given URL could be reached."
  },
  "serverNotReached": "poslužitelj nije dostupan.",
  "@serverNotReached": {
    "description": "status message when the given URL could not be reached."
  },
  "checkURLReachability": "Provjeri dostupnost poslužitelja.",
  "@checkURLReachability": {
    "description": "check wether the server can be reached or not."
  },
  "enterURL": "Upiši adresu poslužitelja!",
  "@enterURL": {
    "description": "prompt that the user hast to enter the server URL."
  },
  "userName": "korisničko ime",
  "@userName": {
    "description": "name of the login user"
  },
  "password": "lozinka",
  "@password": {
    "description": "password"
  },
  "clientPassword": "lozinka klijenta",
  "@clientPassword": {
    "description": "password to secure the client itself"
  },
  "helpMenu": "Pomoć",
  "@helpMenu": {},
  "syncRate": "interval sinkronizacije",
  "@syncRate": {
    "description": "time between object retrieval from server"
  },
  "save": "spremi",
  "@save": {
    "description": "save"
  },
  "about": "informacije",
  "@about": {
    "description": "about the app"
  },
  "cancel": "odustani",
  "@cancel": {
    "description": "cancel"
  },
  "enterUserName": "upiši svoje korisničko ime",
  "@enterUserName": {
    "description": "prompt to enter the username"
  },
  "enterPassword": "upiši svoju lozinku",
  "@enterPassword": {
    "description": "prompt to enter the password"
  },
  "checkLogin": "provjeri ispravnost podataka za prijavu",
  "@checkLogin": {
    "description": "check wether the given credentials are valid or not"
  },
  "saveCredentials": "spremiti podatke za prijavu lokalno?",
  "@saveCredentials": {
    "description": "checkbox if the user wants to save the credentials on local hard drive"
  },
  "continueText": "Nastavi",
  "@continueText": {},
  "enterMasterPassword": "Upiši svoju glavnu lozinku",
  "@enterMasterPassword": {
    "description": "title for popup to enter the masterpassword"
  },
  "saveMasterPassword": "spremiti glavnu lozinku?",
  "@saveMasterPassword": {
    "description": "label for the checkbox if the master password shall be saved"
  },
  "masterPasswordException": "Glavna lozinka je neispravna",
  "@masterPasswordException": {
    "description": "the entered masterpassword is not valid"
  },
  "credentialInputException": "Upisani podaci za prijavu su neispravni",
  "@credentialInputException": {
    "description": "the entered credentials are not valid"
  },
  "enterClientPassword": "Upiši svoju lozinku klijenta",
  "@enterClientPassword": {
    "description": "title for popup to enter the client password"
  },
  "clientPasswordException": "Lozinka klijenta je neispravna",
  "@clientPasswordException": {
    "description": "the entered client password is not valid"
  },
  "ignoreMissingCredentials": "Ne prikazivati ovu poruku ponovo?",
  "@ignoreMissingCredentials": {
    "description": "ignore missing credentials and work with local data if available"
  },
  "enterCredentials": "Upiši svoje podatke za prijavu",
  "@enterCredentials": {
    "description": "title for popup to enter the credentials"
  },
  "nameColumn": "Ime",
  "@nameColumn": {},
  "usernameColumn": "Korisnik",
  "@usernameColumn": {
    "description": "User for the password"
  },
  "passwordColumn": "Lozinka",
  "@passwordColumn": {
    "description": "the password itself"
  },
  "urlColumn": "URL",
  "@urlColumn": {
    "description": "Url to the website of the password"
  },
  "detailsColumn": "Detalji",
  "@detailsColumn": {
    "description": "Details to the password"
  },
  "notesColumn": "Bilješke",
  "@notesColumn": {
    "description": "Notes to the password"
  },
  "revisionColumn": "Revizije",
  "@revisionColumn": {
    "description": "Revisions to the password"
  },
  "sharedColumn": "Dijeljenjo",
  "@sharedColumn": {
    "description": "Is the password shared with other users"
  },
  "statusColumn": "Stanje",
  "@statusColumn": {
    "description": "The passwords security status (secure, unsecure...)"
  },
  "systemMenu": "Sustav",
  "@systemMenu": {
    "description": "String for the menu bar to display system"
  },
  "systemMenuTooltip": "Izbornik za teme povezane sa sustavom",
  "@systemMenuTooltip": {
    "description": "Tooltip for menuButton for system related topics"
  },
  "settingsMenuItem": "Postavke",
  "@settingsMenuItem": {
    "description": "String for the menu item to display settings"
  },
  "helpMenuTooltip": "Zatraži pomoć i daljnje informacije",
  "@helpMenuTooltip": {
    "description": "Tooltip for menuButton for help and further information"
  },
  "onlineHelpMenuItem": "Pomoć na internetu",
  "@onlineHelpMenuItem": {
    "description": "String for the menu item to display Online-Help"
  },
  "donationMenuItem": "Doniraj",
  "@donationMenuItem": {
    "description": "String for the menu item to display Donate"
  },
  "reportIssueMenuItem": "Prijavi problem",
  "@reportIssueMenuItem": {
    "description": "String for the menu item to display Report an issue"
  },
  "aboutMenuItem": "Informacije",
  "@aboutMenuItem": {
    "description": "String for the menu item to display About"
  },
  "accept": "prihvati",
  "@accept": {
    "description": "accept"
  },
  "logout": "odjavi se",
  "@logout": {
    "description": "logout from server"
  },
  "recent": "nedavno korištene",
  "@recent": {
    "description": "recently used passwords"
  },
  "all": "sve",
  "@all": {
    "description": "all passwords"
  },
  "favorites": "favoriti",
  "@favorites": {
    "description": "favorites passwords"
  },
  "shared": "dijeljene",
  "@shared": {
    "description": "shared passwords"
  },
  "tags": "oznake",
  "@tags": {
    "description": "tags of passwords"
  },
  "security": "sigurnost",
  "@security": {
    "description": "security level of passwords"
  },
  "secure": "sigurna",
  "@secure": {
    "description": "security level 'secure' of passwords"
  },
  "weak": "slaba",
  "@weak": {
    "description": "security level 'weak' of passwords"
  },
  "breached": "kompromitirana",
  "@breached": {
    "description": "security level 'breached' of passwords"
  },
  "sharedWithYou": "s tobom dijeljene",
  "@sharedWithYou": {
    "description": "passwords someone shared with you"
  },
  "sharedByYou": "tvoje dijeljene",
  "@sharedByYou": {
    "description": "passwords you shared with someone"
  },
  "passwords": "lozinke",
  "@passwords": {
    "description": "passwords"
  },
  "folder": "mapa",
  "@folder": {
    "description": "folder"
  },
  "dark": "Tamna",
  "@dark": {
    "description": "dark theme"
  },
  "light": "Svijetla",
  "@light": {
    "description": "light theme"
  },
  "custom": "Prilagođeno",
  "@custom": {
    "description": "custom theme"
  },
  "addTag": "dodaj oznaku",
  "@addTag": {
    "description": "add tag to password"
  },
  "expired": "nesigurna (istekla)",
  "@expired": {
    "description": "the password is unsecure because it has not been changed in a long time"
  },
  "duplicate": "nesigurna (duplikat)",
  "@duplicate": {
    "description": "the password is unsecure because it already use elsewhere"
  },
  "sse1r1": "Jednostavno šifriranje SSEv1r1 (Gen. 1)",
  "@sse1r1": {
    "description": "SSEv1r1 Simple encryption (Gen. 1)"
  },
  "ssev1r2": "Jednostavno šifriranje SSEv1r1 (Gen. 2)",
  "@ssev1r2": {
    "description": "SSEv1r2 Simple encryption (Gen. 2)"
  },
  "ssev2": "Napredno šifriranje SSEv2 (SSE V2)",
  "@ssev2": {
    "description": "SSEv2 Advanced encryption (SSE V2)"
  },
  "noEncryption": "Bez šifriranja",
  "@noEncryption": {},
  "serverEncryption": "šifriranje poslužitelja",
  "@serverEncryption": {
    "description": "server encryption"
  },
  "clientEncryption": "šifriranje klijenta",
  "@clientEncryption": {
    "description": "client encryption"
  },
  "id": "ID",
  "@id": {
    "description": "ID"
  },
  "lastEdited": "zadnja promjena",
  "@lastEdited": {
    "description": "last edited"
  },
  "created": "stvoreno",
  "@created": {
    "description": "created"
  },
  "statistics": "statistika",
  "@statistics": {
    "description": "statistical password details"
  },
  "themes": "Teme",
  "@themes": {
    "description": "themes"
  },
  "login": "Prijava",
  "@login": {
    "description": "login"
  },
  "general": "Opće",
  "@general": {
    "description": "general password details"
  },
  "share": "Dijeli",
  "@share": {
    "description": "manage password shares"
  },
  "passwordRemoteUpdateError": "Nije bilo moguće aktualizirati lozinku na serveru.",
  "@passwordRemoteUpdateError": {
    "description": "the password could not be updated by the api. Eventually the local changes will be overwritten when data is received from server in background process"
  },
  "passwordMenu": "Opcije za lozinku",
  "@passwordMenu": {
    "description": "menu for password options"
  },
  "folderMenu": "Opcije za mape",
  "@folderMenu": {
    "description": "menu for folder options"
  },
  "addPassword": "dodaj lozinku",
  "@addPassword": {
    "description": "menu entry for adding a password"
  },
  "editPassword": "uredi lozinku",
  "@editPassword": {
    "description": "menu entry for editing a password"
  },
  "deletePassword": "izbriši lozinku",
  "@deletePassword": {
    "description": "menu entry for deleting a password"
  },
  "addFolder": "dodaj mapu",
  "@addFolder": {
    "description": "menu entry for adding a folder"
  },
  "editFolder": "uredi mapu",
  "@editFolder": {
    "description": "menu entry for editing a folder"
  },
  "deleteFolder": "izbriši mapu",
  "@deleteFolder": {
    "description": "menu entry for deleting a folder"
  },
  "initError": "Greška pri inicijaliziranju programa!",
  "@initError": {
    "description": "There was an error initializing the app. Mostly because of missing dependencies."
  },
  "installationInstructionGeneral": "Slijedi upute instalacije!",
  "@installationInstructionGeneral": {
    "description": "Follow installation instruction to get the app running"
  },
  "installationInstructionWindows": "Instaliraj libsodium u C:\\Windows\\System32",
  "@installationInstructionWindows": {
    "description": "Install libsodium in C:\\Windows\\System32"
  },
  "installationInstructionLinux": "Instaliraj libsodium, libsecret, libjsoncpp i asecret-service!",
  "@installationInstructionLinux": {
    "description": "Install libsodium, libsecret, libjsoncpp and a secret-service"
  },
  "installationInstructionMacOS": "Instaliraj libsodium!",
  "@installationInstructionMacOS": {
    "description": "Install libsodium"
  },
  "confirmRevisionTitle": "Obnoviti verziju?",
  "@confirmRevisionTitle": {
    "description": "Title for dialogue to confirm restoring password to chosen revision"
  },
  "confirmRevisionText": "Želiš li obnoviti ovu verziju?",
  "@confirmRevisionText": {
    "description": "Main text for dialogue to confirm restoring password to chosen revision"
  },
  "restore": "obnovi",
  "@restore": {
    "description": "button for dialouge to restore password to revision"
  },
  "create": "stvori",
  "@create": {
    "description": "create"
  },
  "folderName": "ime mape",
  "@folderName": {
    "description": "Name des Ordners"
  },
  "subfolder": "podmapa",
  "@subfolder": {
    "description": "subfolder"
  },
  "folderRemoteUpdateError": "Nije bilo moguće aktualizirati mapu na serveru.",
  "@folderRemoteUpdateError": {
    "description": "the folder could not be updated by the api. Eventually the local changes will be overwritten when data is received from server in background process"
  },
  "createFolder": "stvori mapu",
  "@createFolder": {
    "description": "menu entry for creating a folder"
  },
  "searchPassword": "traži lozinku",
  "@searchPassword": {
    "description": "Label for password search input field"
  },
  "deleteTag": "izbriši oznaku",
  "@deleteTag": {},
  "searchUser": "traži korisnika",
  "@searchUser": {
    "description": "Label for the user search field"
  },
  "languageSelection": "Odaberi jezik",
  "@languageSelection": {
    "description": "Title for language selection"
  },
  "createTag": "stvori oznaku",
  "@createTag": {
    "description": "menu entry for creating a tag"
  },
  "parentFolder": "nadređena mapa",
  "@parentFolder": {
    "description": "text for the selection of the parent folder"
  },
  "editTag": "uredi oznaku",
  "@editTag": {
    "description": "menu entry for editing a tag"
  },
  "notYetImplemented": "još nije implementirano.",
  "@notYetImplemented": {
    "description": "Notification if some logic is not yet implemented."
  },
  "folderNotDeletable": "Osnovna mapa se ne može izbrisati.",
  "@folderNotDeletable": {
    "description": "Notification if the user wants to edit the root folde"
  },
  "tag": "oznaka",
  "@tag": {
    "description": "tag"
  },
  "createPassword": "stvori lozinku",
  "@createPassword": {
    "description": "menu entry for creating a password"
  },
  "tagMenu": "Opcije za oznake",
  "@tagMenu": {
    "description": "menu for tag options"
  },
  "passwordSharedWithYou": "je dijelio/la ovu lozinku s tobom",
  "@passwordSharedWithYou": {
    "description": "text for the password share information"
  },
  "folderNotEditable": "Osnovna mapa se ne može urediti.",
  "@folderNotEditable": {
    "description": "Notification if the user wants to edit the root folde"
  },
  "localChangesOverwriteHint": "Promjene se spremaju lokalno i možda će se u nekom trenutku prebrisati sa servera!",
  "@localChangesOverwriteHint": {
    "description": "the change could not be processed by the api. Eventually the local changes will be overwritten when data is received from server in background process"
  },
  "rootFolderDeletionError": "Korijenska mapa se ne može izbrisati.",
  "@rootFolderDeletionError": {
    "description": "The root folder can and may not be deleted!"
  },
  "folderRemoteDeletionError": "Nije bilo moguće eksterno izbrisati mapu. ",
  "@folderRemoteDeletionError": {
    "description": "The folder could not be deleted by the API."
  }
}
