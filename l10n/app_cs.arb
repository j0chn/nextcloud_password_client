{
  "masterPasswordException": "Hlavní heslo není správně",
  "@masterPasswordException": {
    "description": "the entered masterpassword is not valid"
  },
  "detailsColumn": "Podrobnosti",
  "@detailsColumn": {
    "description": "Details to the password"
  },
  "serverNotReached": "server není dosažitelný.",
  "@serverNotReached": {
    "description": "status message when the given URL could not be reached."
  },
  "checkURLReachability": "Zkontrolujte za je server dosažitelný.",
  "@checkURLReachability": {
    "description": "check wether the server can be reached or not."
  },
  "enterPassword": "zadejte své heslo",
  "@enterPassword": {
    "description": "prompt to enter the password"
  },
  "checkLogin": "zkontrolujte, zda jsou přihlašovací údaje platné",
  "@checkLogin": {
    "description": "check wether the given credentials are valid or not"
  },
  "saveCredentials": "uložit přihlašovací údaje lokálně?",
  "@saveCredentials": {
    "description": "checkbox if the user wants to save the credentials on local hard drive"
  },
  "continueText": "Pokračovat",
  "@continueText": {},
  "enterMasterPassword": "Zadejte hlavní heslo",
  "@enterMasterPassword": {
    "description": "title for popup to enter the masterpassword"
  },
  "saveMasterPassword": "uložit si hlavní heslo?",
  "@saveMasterPassword": {
    "description": "label for the checkbox if the master password shall be saved"
  },
  "clientPassword": "heslo pro klienta",
  "@clientPassword": {
    "description": "password to secure the client itself"
  },
  "enterClientPassword": "Zadejte heslo do svého klienta",
  "@enterClientPassword": {
    "description": "title for popup to enter the client password"
  },
  "clientPasswordException": "Heslo do klienta není správně",
  "@clientPasswordException": {
    "description": "the entered client password is not valid"
  },
  "ignoreMissingCredentials": "Tuto zprávu už příště nezobrazovat?",
  "@ignoreMissingCredentials": {
    "description": "ignore missing credentials and work with local data if available"
  },
  "enterCredentials": "Zadejte své přihlašovací údaje",
  "@enterCredentials": {
    "description": "title for popup to enter the credentials"
  },
  "usernameColumn": "Uživatel",
  "@usernameColumn": {
    "description": "User for the password"
  },
  "passwordColumn": "Heslo",
  "@passwordColumn": {
    "description": "the password itself"
  },
  "urlColumn": "URL",
  "@urlColumn": {
    "description": "Url to the website of the password"
  },
  "nameColumn": "Název",
  "@nameColumn": {},
  "notesColumn": "Poznámky",
  "@notesColumn": {
    "description": "Notes to the password"
  },
  "revisionColumn": "Revize",
  "@revisionColumn": {
    "description": "Revisions to the password"
  },
  "sharedColumn": "Sdílené",
  "@sharedColumn": {
    "description": "Is the password shared with other users"
  },
  "statusColumn": "Stav",
  "@statusColumn": {
    "description": "The passwords security status (secure, unsecure...)"
  },
  "systemMenu": "Systém",
  "@systemMenu": {
    "description": "String for the menu bar to display system"
  },
  "enterURL": "Zadejte adresu serveru!",
  "@enterURL": {
    "description": "prompt that the user hast to enter the server URL."
  },
  "credentialInputException": "Zadané přihlašovací údaje nejsou správné",
  "@credentialInputException": {
    "description": "the entered credentials are not valid"
  },
  "password": "heslo",
  "@password": {
    "description": "password"
  },
  "createLocalCopy": "ukládat hesla lokálně?",
  "@createLocalCopy": {
    "description": "Check if a local copy shall be created (as fallback in case the server is not reachable)."
  },
  "serverReached": "server je dosažitelný.",
  "@serverReached": {
    "description": "status message when the given URL could be reached."
  },
  "enterUserName": "zadejte své uživatelské jméno",
  "@enterUserName": {
    "description": "prompt to enter the username"
  },
  "userName": "uživatelské jméno",
  "@userName": {
    "description": "name of the login user"
  },
  "passwords": "hesla",
  "@passwords": {
    "description": "passwords"
  },
  "helpMenu": "Nápověda",
  "@helpMenu": {},
  "helpMenuTooltip": "Nápověda a další informace",
  "@helpMenuTooltip": {
    "description": "Tooltip for menuButton for help and further information"
  },
  "onlineHelpMenuItem": "Nápověda na webu",
  "@onlineHelpMenuItem": {
    "description": "String for the menu item to display Online-Help"
  },
  "donationMenuItem": "Podpořit vývoj darem",
  "@donationMenuItem": {
    "description": "String for the menu item to display Donate"
  },
  "reportIssueMenuItem": "Nahlásit problém",
  "@reportIssueMenuItem": {
    "description": "String for the menu item to display Report an issue"
  },
  "aboutMenuItem": "O aplikaci",
  "@aboutMenuItem": {
    "description": "String for the menu item to display About"
  },
  "accept": "přijmout",
  "@accept": {
    "description": "accept"
  },
  "logout": "odhlásit",
  "@logout": {
    "description": "logout from server"
  },
  "settingsMenuItem": "Nastavení",
  "@settingsMenuItem": {
    "description": "String for the menu item to display settings"
  },
  "about": "o aplikaci",
  "@about": {
    "description": "about the app"
  },
  "all": "vše",
  "@all": {
    "description": "all passwords"
  },
  "favorites": "oblíbené",
  "@favorites": {
    "description": "favorites passwords"
  },
  "shared": "sdílené",
  "@shared": {
    "description": "shared passwords"
  },
  "tags": "štítky",
  "@tags": {
    "description": "tags of passwords"
  },
  "weak": "slabé",
  "@weak": {
    "description": "security level 'weak' of passwords"
  },
  "breached": "prolomené",
  "@breached": {
    "description": "security level 'breached' of passwords"
  },
  "sharedWithYou": "nasdíleno vám",
  "@sharedWithYou": {
    "description": "passwords someone shared with you"
  },
  "sharedByYou": "nasdílené vámi",
  "@sharedByYou": {
    "description": "passwords you shared with someone"
  },
  "custom": "Uživatelsky určené",
  "@custom": {
    "description": "custom theme"
  },
  "addTag": "přidat štítek",
  "@addTag": {
    "description": "add tag to password"
  },
  "expired": "nebezpečné (platnost skončila)",
  "@expired": {
    "description": "the password is unsecure because it has not been changed in a long time"
  },
  "duplicate": "nebezpečené (figuruje na více místech)",
  "@duplicate": {
    "description": "the password is unsecure because it already use elsewhere"
  },
  "installationInstructionWindows": "Nainstalujte libsodium do C:\\Windows\\System32",
  "@installationInstructionWindows": {
    "description": "Install libsodium in C:\\Windows\\System32"
  },
  "installationInstructionGeneral": "Postupujte podle instalačních pokynů!",
  "@installationInstructionGeneral": {
    "description": "Follow installation instruction to get the app running"
  },
  "initError": "Došlo k chybě při inicializaci aplikace!",
  "@initError": {
    "description": "There was an error initializing the app. Mostly because of missing dependencies."
  },
  "deleteFolder": "smazat složku",
  "@deleteFolder": {
    "description": "menu entry for deleting a folder"
  },
  "editFolder": "upravit složku",
  "@editFolder": {
    "description": "menu entry for editing a folder"
  },
  "addFolder": "přidat složku",
  "@addFolder": {
    "description": "menu entry for adding a folder"
  },
  "deletePassword": "smazat heslo",
  "@deletePassword": {
    "description": "menu entry for deleting a password"
  },
  "folderMenu": "Možnosti složky",
  "@folderMenu": {
    "description": "menu for folder options"
  },
  "passwordMenu": "Možnosti hesla",
  "@passwordMenu": {
    "description": "menu for password options"
  },
  "passwordRemoteUpdateError": "Heslo se nepodařilo aktualizovat na serveru.",
  "@passwordRemoteUpdateError": {
    "description": "the password could not be updated by the api. Eventually the local changes will be overwritten when data is received from server in background process"
  },
  "share": "Nasdílet",
  "@share": {
    "description": "manage password shares"
  },
  "general": "Obecné",
  "@general": {
    "description": "general password details"
  },
  "login": "Přihlásit",
  "@login": {
    "description": "login"
  },
  "themes": "Motivy vzhledu",
  "@themes": {
    "description": "themes"
  },
  "statistics": "statistiky",
  "@statistics": {
    "description": "statistical password details"
  },
  "noEncryption": "Bez šifrování",
  "@noEncryption": {},
  "sse1r1": "SSEv1r1 jednoduché šifrování (1. gen)",
  "@sse1r1": {
    "description": "SSEv1r1 Simple encryption (Gen. 1)"
  },
  "ssev1r2": "SSEv1r2 jednoduché šifrování (2. gen)",
  "@ssev1r2": {
    "description": "SSEv1r2 Simple encryption (Gen. 2)"
  },
  "ssev2": "SSEv2 pokročilé šifrování (SSE V2)",
  "@ssev2": {
    "description": "SSEv2 Advanced encryption (SSE V2)"
  },
  "cancel": "storno",
  "@cancel": {
    "description": "cancel"
  },
  "restore": "obnovit",
  "@restore": {
    "description": "button for dialouge to restore password to revision"
  },
  "confirmRevisionText": "Opravdu chcete vrátit do podoby této revize?",
  "@confirmRevisionText": {
    "description": "Main text for dialogue to confirm restoring password to chosen revision"
  },
  "confirmRevisionTitle": "Obnovit verzi?",
  "@confirmRevisionTitle": {
    "description": "Title for dialogue to confirm restoring password to chosen revision"
  },
  "installationInstructionMacOS": "Je třeba nainstalovat knihovnu libsodium!",
  "@installationInstructionMacOS": {
    "description": "Install libsodium"
  },
  "installationInstructionLinux": "Nainstalujte libsodium, libsecret, libjsoncpp a asecret-service!",
  "@installationInstructionLinux": {
    "description": "Install libsodium, libsecret, libjsoncpp and a secret-service"
  },
  "systemMenuTooltip": "Nabídka pro témata související se systémem",
  "@systemMenuTooltip": {
    "description": "Tooltip for menuButton for system related topics"
  },
  "light": "Světlý",
  "@light": {
    "description": "light theme"
  },
  "syncRate": "interval synchronizace",
  "@syncRate": {
    "description": "time between object retrieval from server"
  },
  "save": "uložit",
  "@save": {
    "description": "save"
  },
  "security": "zabezpečení",
  "@security": {
    "description": "security level of passwords"
  },
  "folder": "složka",
  "@folder": {
    "description": "folder"
  },
  "dark": "Tmavý",
  "@dark": {
    "description": "dark theme"
  },
  "recent": "nedávno použité",
  "@recent": {
    "description": "recently used passwords"
  },
  "secure": "bezpečné",
  "@secure": {
    "description": "security level 'secure' of passwords"
  },
  "addPassword": "přidat heslo",
  "@addPassword": {
    "description": "menu entry for adding a password"
  },
  "created": "vytvořeno",
  "@created": {
    "description": "created"
  },
  "lastEdited": "naposledy upraveno",
  "@lastEdited": {
    "description": "last edited"
  },
  "id": "Identif.",
  "@id": {
    "description": "ID"
  },
  "editPassword": "upravit heslo",
  "@editPassword": {
    "description": "menu entry for editing a password"
  },
  "clientEncryption": "šifrování na straně klienta",
  "@clientEncryption": {
    "description": "client encryption"
  },
  "serverEncryption": "šifrování na straně serveru",
  "@serverEncryption": {
    "description": "server encryption"
  },
  "folderRemoteDeletionError": "Složku není možné smazat na dálku. ",
  "@folderRemoteDeletionError": {
    "description": "The folder could not be deleted by the API."
  },
  "localChangesOverwriteHint": "Změny jsou ukládány lokálně a mohou být někdy přepsány ze serveru!",
  "@localChangesOverwriteHint": {
    "description": "the change could not be processed by the api. Eventually the local changes will be overwritten when data is received from server in background process"
  },
  "folderRemoteUpdateError": "Složku nebylo možné zaktualizovat na dálku.",
  "@folderRemoteUpdateError": {
    "description": "the folder could not be updated by the api."
  },
  "tagMenu": "Předvolby štítků",
  "@tagMenu": {
    "description": "menu for tag options"
  },
  "createPassword": "vytvořit heslo",
  "@createPassword": {
    "description": "menu entry for creating a password"
  },
  "tag": "štítek",
  "@tag": {
    "description": "tag"
  },
  "deleteTag": "smazat štítek",
  "@deleteTag": {
    "description": "menu entry for deleting a tag"
  },
  "editTag": "upravit štítek",
  "@editTag": {
    "description": "menu entry for editing a tag"
  },
  "createTag": "vytvořit štítek",
  "@createTag": {
    "description": "menu entry for creating a tag"
  },
  "create": "vytvořit",
  "@create": {
    "description": "create"
  },
  "folderName": "název složky",
  "@folderName": {
    "description": "name of the folder"
  },
  "languageSelection": "Zvolte jazyk",
  "@languageSelection": {
    "description": "Title for language selection"
  },
  "searchPassword": "hledat heslo",
  "@searchPassword": {
    "description": "Label for password search input field"
  },
  "searchUser": "hledat uživatele",
  "@searchUser": {
    "description": "Label for the user search field"
  },
  "passwordSharedWithYou": "vám nasdíle(a) toto heslo",
  "@passwordSharedWithYou": {
    "description": "text for the password share information"
  },
  "parentFolder": "nadřazená složka",
  "@parentFolder": {
    "description": "text for the selection of the parent folder"
  },
  "folderNotEditable": "Kořenovou složku není možné upravit.",
  "@folderNotEditable": {
    "description": "Notification if the user wants to edit the root folde"
  },
  "folderNotDeletable": "Kořenovou složku není možné smazat.",
  "@folderNotDeletable": {
    "description": "Notification if the user wants to edit the root folde"
  },
  "rootFolderDeletionError": "Kořenovou složku není možné smazat.",
  "@rootFolderDeletionError": {
    "description": "The root folder can and may not be deleted!"
  },
  "createFolder": "vytvořit složku",
  "@createFolder": {
    "description": "menu entry for creating a folder"
  },
  "subfolder": "podsložka",
  "@subfolder": {
    "description": "subfolder"
  },
  "notYetImplemented": "zatím neimplementováno.",
  "@notYetImplemented": {
    "description": "Notification if some logic is not yet implemented."
  }
}
