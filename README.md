# Nexctloud Password Client

## Status

[![pipeline status](https://gitlab.com/j0chn/nextcloud_password_client/badges/main/pipeline.svg)](https://gitlab.com/j0chn/nextcloud_password_client/-/commits/main) [![coverage report](https://gitlab.com/j0chn/nextcloud_password_client/badges/main/coverage.svg)](https://gitlab.com/j0chn/nextcloud_password_client/-/commits/main) [![Lines of code](https://img.shields.io/tokei/lines/gitlab.com/j0chn/nextcloud_password_client)]()\
[![Discord](https://img.shields.io/discord/924751179991363694)](https://discord.gg/C6PbpjPYB7)

<a href="https://hosted.weblate.org/engage/nextcloud_password_client/"> <img src="https://hosted.weblate.org/widgets/nextcloud_password_client/-/287x66-grey.png" alt="Translation status" /> </a>

## About the app

Nextcloud Passwords Client is a Flutter based application to manage passwords from the corresponding [Nextcloud Passwords](https://apps.nextcloud.com/apps/passwords) app. The focus is set to desktop clients, especially linux.<p/> <img src= "desktop_files/npc_logo.svg" height="200" width="200"/>

### Screenshots
<img src="assets/login_screen.jpg" height="200" width="200"/>
<img src="assets/main_screen.jpg" height="200" width="200"/>
<img src="assets/navigation_tree.jpg" height="200" width="200"/>
<img src="assets/password_grid.jpg" height="200" width="200"/>
<img src="assets/password_search.jpg" height="200" width="200"/>
<img src="assets/password_details.jpg" height="200" width="200"/>
<img src="assets/password_notes.jpg" height="200" width="200"/>
<img src="assets/password_sharing.jpg" height="200" width="200"/>
<img src="assets/password_revisions.jpg" height="200" width="200"/>
<img src="assets/language_support.jpg" height="200" width="200"/>

## Get it from

<a href="https://www.microsoft.com/store/productId/9NXVZ0ZP6D5Z" target="_blank"><img src="https://static.wikia.nocookie.net/logopedia/images/4/48/Badge_windowsstore.png/revision/latest/scale-to-width-down/202?cb=20160422152629" alt="Download from Microsoft store" height="77" width="190"></a> <a href='https://flathub.org/apps/details/com.gitlab.j0chn.nextcloud_password_client'><img height='65' width='180' alt='Download on Flathub' src='https://flathub.org/assets/badges/flathub-badge-en.png'/></a>

For windows you additionally need to:

1. Download and install [Microsoft Visual C++ Redistributable](https://aka.ms/vs/17/release/vc_redist.x64.exe).
2. Download [Libsodium 1.0.18](https://download.libsodium.org/libsodium/releases/libsodium-1.0.18-stable-msvc.zip) and place the `x64/Release/v143/dynamic/libsodium.dll` in your `C:\\Windows\System32` folder.

## Attention

If you are coming from a version before November 30th 2023 you may need to delete the files within the "your documents"/nextcloud\_password\_client/ folder. If you do not do this the app may not start.\
Reason is a deleted attribute.

This app is under development and in an early state. A lot of functionality is missing and the UI is not yet optimized.\

## Features

Please check the [roadmap](https://gitlab.com/j0chn/nextcloud_password_client/-/wikis/Roadmap) and [feature list](https://gitlab.com/j0chn/nextcloud_password_client/-/wikis/Features)\
Or requests a [feature](https://gitlab.com/j0chn/nextcloud_password_client/-/wikis/Request-a-feature)

## bugs

Please create an [issue](https://gitlab.com/j0chn/nextcloud_password_client/-/wikis/Report-a-bug)

## Support

Feel free to\
<a href="https://www.ko-fi.com/j0chn" target="\_blank"><img src="https://cdn.ko-fi.com/cdn/kofi2.png?v=3" alt="Buy Me A Ko-Fi" height="41" width="174"></a>

To help me translating the app, please go to\
<a href = "https://hosted.weblate.org/projects/nextcloud_password_client/translations/" target="\_blank"><img src ="https://s.weblate.org/cdn/Logo-Darktext-borders.png" height="41" width="174"></a>).

Thank you.

## Thanks to

* [Jonas Blatt](https://gitlab.com/joleaf/nc-passwords-app/) who provided some templates when I did not progress.
* Flutter Community supporting me when I stuck
  * especially Ismail with his helpful attitude and endless patience ;-)
* Marius David Wieschollek helping me with API problems
* My colleague providing the logo
* Flathub team fixing my config!

##Contact
[Contact me via xmpp](xmpp:nextcloud-password-client@chat.disroot.org?join)   
[Contact me via Issue Tracker](https://gitlab.com/j0chn/nextcloud_password_client/-/issues)